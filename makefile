include .env

.EXPORT_ALL_VARIABLES:

.PHONY: install_deps
install_deps:
	go mod download
	go install github.com/swaggo/swag/cmd/swag@v1.7.4

.PHONY: build	
build:
	swag init -g ./cmd/api/main.go
	go build -o ./$(GOCONFIG_SERVICENAME) cmd/api/main.go

.PHONY: swag	
swag:
	# export PATH=$(go env GOPATH)/bin:$PATH
	swag init -g ./cmd/api/main.go

.PHONY: upload
upload:
	docker-compose up --build

.PHONY: tidy	
tidy:
	go mod tidy