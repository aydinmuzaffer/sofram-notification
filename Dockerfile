FROM golang:1.18.2-alpine3.16 as build-env

RUN apk update \
    && apk upgrade \
    && apk add --no-cache git bash make


# Move to working directory (/build).
WORKDIR /build

# Copy and download dependency using go mod.
COPY go.mod go.sum ./

# Copy the code into the container.
COPY . .

RUN CGO_ENABLED=0 GOOS=linux 
RUN make install_api_deps
RUN make build_api

# Final docker image
FROM alpine:3.7

WORKDIR /

COPY --from=build-env /build .

CMD ["./notification-api"]