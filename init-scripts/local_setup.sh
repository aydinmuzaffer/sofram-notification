#!/bin/sh
set -x

S3_BUCKET=local-notification-demo
REGION=us-east-1

function fail() {
    echo $2
    exit $1
}

echo "Creating S3 bucket: $S3_BUCKET"
awslocal --endpoint-url=http://localhost:4566 s3 mb s3://${S3_BUCKET} --region ${REGION} 

[ $? == 0 ] || fail 1 "Failed: AWS / bucket / create-bucket"


set +