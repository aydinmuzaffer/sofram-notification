package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/aydinmuzaffer/sofram-notification/internal/db"
	"github.com/aydinmuzaffer/sofram-notification/internal/helper/middlewares"
	"github.com/aydinmuzaffer/sofram-notification/internal/httphandlers"

	"github.com/aydinmuzaffer/sofram-notification/internal/cloud"
	"github.com/aydinmuzaffer/sofram-notification/internal/repos"
	services "github.com/aydinmuzaffer/sofram-notification/internal/services"

	_ "github.com/aydinmuzaffer/sofram-notification/docs"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	log "github.com/sirupsen/logrus"
)

func Run() error {
	log.SetFormatter(&log.JSONFormatter{})
	log.Info("Setting Up Our APP")

	dbHost, ok := os.LookupEnv("DB_HOST")
	if !ok {
		log.Error("missing DB_HOST environment variable")
		return errors.New("missing DB_HOST environment variable")
	}
	dbPort, ok := os.LookupEnv("DB_PORT")
	if !ok {
		log.Error("missing DB_PORT environment variable")
		return errors.New("missing DB_PORT environment variable")
	}
	dbUser, ok := os.LookupEnv("DB_USER")
	if !ok {
		log.Error("missing DB_USER environment variable")
		return errors.New("missing DB_USER environment variable")
	}
	dbName, ok := os.LookupEnv("DB_NAME")
	if !ok {
		log.Error("missing DB_NAME environment variable")
		return errors.New("missing DB_NAME environment variable")
	}
	dbPassword, ok := os.LookupEnv("DB_PASSWORD")
	if !ok {
		log.Error("missing DB_PASSWORD environment variable")
		return errors.New("missing DB_PASSWORD environment variable")
	}
	dbSslMode, ok := os.LookupEnv("SSL_MODE")
	if !ok {
		log.Error("missing SSL_MODE environment variable")
		return errors.New("missing SSL_MODE environment variable")
	}
	bucket, ok := os.LookupEnv("AWS_BUCKET")
	if !ok {
		log.Error("missing AWS_BUCKET environment variable")
		return errors.New("missing AWS_BUCKET environment variable")
	}
	region, ok := os.LookupEnv("AWS_REGION")
	if !ok {
		log.Error("missing AWS_REGION environment variable")
		return errors.New("missing AWS_REGION environment variable")
	}
	endpoint := os.Getenv("AWS_ENDPOINT")
	profile := os.Getenv("AWS_PROFILE")

	notificationCon, err := db.NewNotificationConnection(dbHost, dbPort, dbUser, dbName, dbPassword, dbSslMode)
	if err != nil {
		log.Error("failed to setup connection to the database")
		return err
	}

	db, err := notificationCon.Db.DB()
	if err != nil {
		log.Error("failed to setup database")
		return err
	}
	defer db.Close()

	if err := db.PingContext(context.Background()); err != nil {
		log.Error("failed to ping database")
		return err
	}

	seedRepository := repos.NewSeedRepository(notificationCon.Db)

	if err := seedRepository.MigrateDB(); err != nil {
		log.Error("failed to migrate database")
		return err
	}

	if err := seedRepository.SeedDB(); err != nil {
		log.Error("failed to seed database")
		return err
	}

	log.Info("setting up our handler")

	awsSess, err := cloud.NewAwsSession(profile, endpoint, region)
	if err != nil {
		log.Error("failed to init aws session")
		return err
	}
	log.Info(fmt.Sprintf("Aws session created with profile: %s, endpoint: %s, region: %s", profile, endpoint, region))

	s3Handler := cloud.NewS3Handler(awsSess, bucket)

	chanelRepo := repos.NewChanelRepository()
	chanelService := services.NewChanelService(chanelRepo)
	chanelHandler := httphandlers.NewChanelHandler(chanelService)

	providerRepo := repos.NewProviderRepository()
	providerService := services.NewProviderService(providerRepo, chanelRepo)
	providerHandler := httphandlers.NewProviderHandler(providerService)

	recipientTypeRepo := repos.NewRecipientTypeRepository()
	recipientTypeService := services.NewRecipientTypeService(recipientTypeRepo)
	recipientTypeHandler := httphandlers.NewRecipientTypeHandler(recipientTypeService)

	categoryRepo := repos.NewCategoryRepository(notificationCon.Db)
	categoryService := services.NewCategoryService(categoryRepo)
	categoryHandler := httphandlers.NewCategoryHandler(categoryService)

	emailLayoutRepo := repos.NewEmailLayoutRepository(notificationCon.Db)
	emailLayoutService := services.NewEmailLayoutService(emailLayoutRepo, s3Handler)
	emailLayoutHandler := httphandlers.NewEmailLayoutHandler(emailLayoutService)

	notificationTypeRepo := repos.NewNotificationTypeRepository(notificationCon.Db)
	notificationTypeService := services.NewNotificationTypeService(notificationTypeRepo)
	notificationTypeHandler := httphandlers.NewNotificationTypeHandler(notificationTypeService)

	templateRepo := repos.NewTemplateRepository(notificationCon.Db)
	templateService := services.NewTemplateService(templateRepo)
	templateHandler := httphandlers.NewTemplateHandler(templateService)

	emailTemplateRepo := repos.NewEmailTemplateRepository(notificationCon.Db)
	emailTemplateService := services.NewEmailTemplateService(emailTemplateRepo, s3Handler, templateRepo)
	emailTemplateHandler := httphandlers.NewEmailTemplateHandler(emailTemplateService)

	smsTemplateRepo := repos.NewSmsTemplateRepository(notificationCon.Db)
	smsTemplateService := services.NewSmsTemplateService(smsTemplateRepo)
	smsTemplateHandler := httphandlers.NewSmsTemplateHandler(smsTemplateService)

	languageRepo := repos.NewLanguageRepository(notificationCon.Db)
	languageervice := services.NewLanguageService(languageRepo)
	languageHandler := httphandlers.NewLanguageHandler(languageervice)

	healthCheckHandler := httphandlers.NewPostgresHealthCheckHandler(notificationCon.Db)

	router := gin.Default()

	// appPort := os.Getenv("APP_PORT")
	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", "8080"),
		Handler: router,
	}

	v1 := router.Group("/api/v1")
	{
		chanels := v1.Group("/chanels")
		{
			chanels.GET("", chanelHandler.Get)
		}

		providers := v1.Group("/providers")
		{
			providers.GET("", providerHandler.Get)
		}

		recipientTypes := v1.Group("/recipient-types")
		{
			recipientTypes.GET("", recipientTypeHandler.Get)
		}

		categories := v1.Group("/categories")
		{
			categories.GET("", categoryHandler.Get)
			categories.GET(":id", middlewares.ValidateGuidsFromPath("id"), categoryHandler.GetById)
			categories.POST("", categoryHandler.Add)
			categories.PUT("", categoryHandler.Update)
			categories.DELETE(":id", middlewares.ValidateGuidsFromPath("id"), categoryHandler.Delete)
		}

		emailLayouts := v1.Group("/email-layouts")
		{
			emailLayouts.GET("", emailLayoutHandler.Get)
			emailLayouts.GET(":id", middlewares.ValidateGuidsFromPath("id"), emailLayoutHandler.GetById)
			emailLayouts.POST("", emailLayoutHandler.Add)
			emailLayouts.PUT("", emailLayoutHandler.Update)
			emailLayouts.DELETE(":id", middlewares.ValidateGuidsFromPath("id"), emailLayoutHandler.Delete)
		}

		notificationTypes := v1.Group("/notification-types")
		{
			notificationTypes.GET("", notificationTypeHandler.Get)
			notificationTypes.GET(":id", middlewares.ValidateGuidsFromPath("id"), notificationTypeHandler.GetById)
			notificationTypes.POST("", notificationTypeHandler.Add)
			notificationTypes.PUT("", notificationTypeHandler.Update)
			notificationTypes.DELETE(":id", middlewares.ValidateGuidsFromPath("id"), notificationTypeHandler.Delete)
		}

		templates := v1.Group("/templates")
		{
			templates.GET(":id", middlewares.ValidateGuidsFromPath("id"), templateHandler.GetById)
			templates.DELETE("", templateHandler.Delete)
		}

		emailTemplates := v1.Group("/email-templates")
		{
			emailTemplates.GET(":notificationTypeId", middlewares.ValidateGuidsFromPath("notificationTypeId"), emailTemplateHandler.GetByNotificationId)
			emailTemplates.POST("", emailTemplateHandler.Add)
			emailTemplates.PUT("", emailTemplateHandler.Update)
			emailTemplates.DELETE(":id", middlewares.ValidateGuidsFromPath("id"), emailTemplateHandler.Delete)
		}

		smsTemplates := v1.Group("/sms-templates")
		{
			smsTemplates.GET(":notificationTypeId", middlewares.ValidateGuidsFromPath("notificationTypeId"), smsTemplateHandler.GetByNotificationId)
			smsTemplates.POST("", smsTemplateHandler.Add)
			smsTemplates.PUT("", smsTemplateHandler.Update)
			smsTemplates.DELETE(":id", middlewares.ValidateGuidsFromPath("id"), smsTemplateHandler.Delete)
		}

		languages := v1.Group("/languages")
		{
			languages.GET("", languageHandler.Get)
			languages.GET(":id", middlewares.ValidateGuidsFromPath("id"), languageHandler.GetById)
			languages.POST("", languageHandler.Add)
			languages.PUT("", languageHandler.Update)
			languages.DELETE("", languageHandler.Delete)
		}
	}

	router.GET("/health", healthCheckHandler.Live)
	router.GET("/health/live", healthCheckHandler.Live)
	router.GET("/health/ready", healthCheckHandler.Ready)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := server.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Fatal("Server ListenAndServe error")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 2)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Info("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown")
	}

	log.Info("Server exiting.")

	return nil
}

// @title Sofram Notification API
// @version 1.0
// @BasePath  /api/v1
func main() {
	if err := Run(); err != nil {
		log.Error(err)
		log.Fatal("Error starting up our REST API")
	}
}
