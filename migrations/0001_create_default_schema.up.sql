begin;
create extension if not exists "uuid-ossp";

create table if not exists "category" (
id uuid primary key not null unique,
"name" text,
"description" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "chanel" (
id uuid primary key not null unique,
"name" text,
"description" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "provider" (
id uuid primary key not null unique,
"name" text,
"description" text,
chanel_id uuid,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "notification_type" (
id uuid primary key not null unique,
"name" text,
"description" text,
category_id uuid,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "language" (
id uuid primary key not null unique,
"name" text,
code varchar(3),
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "template" (
id uuid primary key not null unique,
notification_type_id uuid,
"name" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "email_layout" (
id uuid primary key not null unique,
"name" text,
"description" text,
bucket_key text, 
"location" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "email_template" (
id uuid primary key not null unique,
template_id uuid,
email_layout_id uuid,
provider_id uuid,
bucket_key text,
"location" text,
language_id uuid,
"subject" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "sms_template" (
id uuid primary key not null unique,
template_id uuid,
provider_id uuid,
content text,
language_id uuid,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "notification" (
id uuid primary key not null unique,
notification_type_id uuid,
"name" text,
"subject" text,
language_id uuid,
provider_id uuid,
chanel_id uuid,
bucket_key text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "email_attachment" (
id uuid primary key not null unique,
notification_id uuid,
"name" text,
bucket_key text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "transaction" (
id uuid primary key not null unique,
notification_id uuid,
provider_return_code varchar(10),
provider_message text,
provider_message_id varchar(100),
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "recipient_type" (
id uuid primary key not null unique,
"name" text,
"description" text,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

create table if not exists "transaction_recipient" (
id uuid primary key not null unique,
transaction_id uuid,
recipient text,
recipient_type_id uuid,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);

commit;