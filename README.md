# Sofram Notification API

This api is for sending email and sms notification to desired recipients thorugh various providers like AWS SES, MobilDev, Twillo, Gmail. After a new provider integrration is done on the code base you can send notification through this new provider. 

Api also serves endpoints to create notification related datas like email/sms templates, notification categories and notification types.


## Features

- CRUD for notification types
- CRUD for mail notification layouts to be used for multiple mail definitions
- CRUD for mail template to be used as mail body in a mail notification and specify for which notification type the mail template will be used for
- CRUD for sms template to be used as sms body in a sms notification and specify for which notification type the sms template will be used for
- Send mails
- Send sms

## Tech

Sofram-Notification-Api uses a number of AWS cloud techs to work properly:

- [S3] - Aws S3 Bucket; used for storing email, sms tepmplates and notification content like content it self and attachements if any for emails
- [Postgres] - Database of choice to store all the definition data and notification requests
- [PgAdmin] - Postgres PgAdmin used for local development
- [LocalStack] - Cloud emulator for AWS services, used to test various AWS services locally
- [Gorm] - Database ORM of choice
- [golang-migrate] - A golang library for database migrations on startup thorugh sql scripts
- [Docker] - Docker for containerization of the api
- [docker-compose] - Used for to be able to run multiple services locally like s3, postgres, pgadmin and api itself.
- [Make] - For easily organizing code compilation
- [Swago] - A golang swagger library to server api endpoint documentation
- [Golang] - The development languge of choice.


## Installation

Api requires [Golang](https://go.dev/) v1.18+ to run.

Clone the api somewhere in your computer

```sh
cd sofram-notification
make upload
sh local_setup.sh
```

Now browse http://localhost:8080/swagger/index.html and you should see the functining endpoints. 

To stop the service and deleting volumes 
```sh
docker-compose down -v
```