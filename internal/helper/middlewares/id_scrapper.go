package middlewares

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"

	"github.com/gin-gonic/gin"
)

func respondWithError(c *gin.Context, code int, message interface{}) {
	c.AbortWithStatusJSON(code, gin.H{"message": message})
}

func ValidateGuidsFromPath(keys ...string) gin.HandlerFunc {
	// can do some initizalitions from here
	return func(c *gin.Context) {
		for _, k := range keys {
			id := c.Param(k)
			_, err := uuid.FromString(id)
			if err != nil {
				message := fmt.Sprintf("%s parameter is not in a correct form", k)
				respondWithError(c, http.StatusUnprocessableEntity, message)
				return
			}
		}
		c.Next()
	}
}
