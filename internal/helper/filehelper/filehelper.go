package filehelper

import (
	"fmt"
	"mime/multipart"
)

func GetFileContentType(file *multipart.FileHeader) (string, error) {
	header := file.Header
	// Take a look at for instance
	// https://en.wikipedia.org/wiki/MIME#Multipart_messages
	// You are looking for Content-Type...
	// However, it can be multivalued. That's why you get a splice.
	types, ok := header["Content-Type"]
	var contentType string
	if ok {
		// This should be true!
		for _, x := range types {
			contentType = x
			// Most usually you will probably see only one
		}
	} else {
		return "", fmt.Errorf("can't get file content-type")
	}
	return contentType, nil
}
