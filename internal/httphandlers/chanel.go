package httphandlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram-notification/internal/services"

	"github.com/gin-gonic/gin"
)

type ChanelHandler interface {
	Get(c *gin.Context)
}

type chanelHandler struct {
	chanelService services.ChanelService
}

func NewChanelHandler(chanelService services.ChanelService) ChanelHandler {
	return &chanelHandler{chanelService: chanelService}
}

// GetChanels godoc
// @ID get-chanels
// @Summary Get Chanels
// @Description  This endpoint serves for getting all the chanels
// @Tags	chanels
// @Accept  json
// @Produce  json
// @Success      200  {object}  ResponseModel{data=[]services.ChanelGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /chanels [get]
func (r *chanelHandler) Get(c *gin.Context) {

	chanel, err := r.chanelService.Get(c)

	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}
	res := ResponseModel{
		Data: chanel,
	}
	c.JSON(http.StatusOK, res)
}
