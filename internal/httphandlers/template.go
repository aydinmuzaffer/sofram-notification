package httphandlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram-notification/internal/services"
	uuid "github.com/satori/go.uuid"

	"github.com/gin-gonic/gin"
)

type TemplateHandler interface {
	GetById(c *gin.Context)
	Delete(c *gin.Context)
}

type templateHandler struct {
	templateService services.TemplateService
}

func NewTemplateHandler(templateService services.TemplateService) TemplateHandler {
	return &templateHandler{templateService: templateService}
}

// GetTemplateById godoc
// @ID get-template
// @Summary Get Template
// @Description  This endpoint serves for getting a template
// @Tags	templates
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "ID"
// @Success      200  {object}  ResponseModel{data=services.TemplateQueryModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /templates/{id} [get]
func (r *templateHandler) GetById(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))
	template, err := r.templateService.GetById(c, id)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: template,
	}
	c.JSON(http.StatusOK, res)
}

// DeleteTemplate godoc
// @ID delete-template
// @Summary Delete Template
// @Description  This endpoint serves for deleting a template
// @Tags	templates
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "Template ID"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /templates/{id}  [delete]
func (r *templateHandler) Delete(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))
	if err := r.templateService.Delete(c, id); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	c.Status(http.StatusOK)
}
