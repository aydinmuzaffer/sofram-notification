package httphandlers

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

const pingTimeout = 3

type HealthCheckHandler interface {
	Live(*gin.Context)
	Ready(*gin.Context)
}

type postgresHealthCheckHandler struct {
	Db *gorm.DB
}

func NewPostgresHealthCheckHandler(db *gorm.DB) HealthCheckHandler {
	return &postgresHealthCheckHandler{Db: db}
}

func (h *postgresHealthCheckHandler) Live(c *gin.Context) {
	c.Status(http.StatusNoContent)
}

func (h *postgresHealthCheckHandler) Ready(c *gin.Context) {

	ctxPing, cancelPing := context.WithTimeout(c.Request.Context(), time.Duration(pingTimeout)*time.Second)
	defer cancelPing()

	db, err := h.Db.DB()
	if err != nil {
		c.JSON(http.StatusRequestTimeout, fmt.Errorf("db ping timeout with %s", err.Error()))
	}

	if err := db.PingContext(ctxPing); err != nil {
		c.JSON(http.StatusRequestTimeout, fmt.Errorf("db ping timeout with %s", err.Error()))
	}

	c.Status(http.StatusNoContent)
}
