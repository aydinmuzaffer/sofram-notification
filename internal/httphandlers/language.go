package httphandlers

import (
	"fmt"
	"net/http"

	"github.com/aydinmuzaffer/sofram-notification/internal/services"
	uuid "github.com/satori/go.uuid"

	"github.com/gin-gonic/gin"
)

type LanguageHandler interface {
	Get(c *gin.Context)
	GetById(c *gin.Context)
	Add(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
}

type languageHandler struct {
	languageService services.LanguageService
}

func NewLanguageHandler(languageService services.LanguageService) LanguageHandler {
	return &languageHandler{languageService: languageService}
}

// GetLanguages godoc
// @ID get-languages
// @Summary Get Languages
// @Description  This endpoint serves for getting all the languages
// @Tags	languages
// @Accept  json
// @Produce  json
// @Param        code   query      string  false  "Langauge Code"
// @Success      200  {object}  ResponseModel{data=[]services.LanguageGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /languages [get]
func (r *languageHandler) Get(c *gin.Context) {

	code := c.Query("code")
	languages, err := r.languageService.Get(c, code)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: languages,
	}
	c.JSON(http.StatusOK, res)
}

// GetCategor godoc
// @ID get-language
// @Summary Get Language
// @Description  This endpoint serves for getting a language
// @Tags	languages
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "Language ID"
// @Success      200  {object}  ResponseModel{data=services.LanguageGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /languages/{id} [get]
func (r *languageHandler) GetById(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))
	language, err := r.languageService.GetById(c, id)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: language,
	}
	c.JSON(http.StatusOK, res)
}

// AddLanguage godoc
// @ID add-language
// @Summary Add Language
// @Description  This endpoint serves for adding a language
// @Tags	languages
// @Accept  json
// @Produce  json
// @Param language body services.LanguageAddModel  true  "Language Data"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /languages [post]
func (r *languageHandler) Add(c *gin.Context) {

	var newLanguage services.LanguageAddModel

	if err := c.BindJSON(&newLanguage); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	language, err := r.languageService.Add(c, &newLanguage)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	res := ResponseModel{
		Data: language,
	}
	c.JSON(http.StatusOK, res)
}

// UpdateLanguage godoc
// @ID update-language
// @Summary Add Language
// @Description  This endpoint serves for updating a language
// @Tags	languages
// @Accept  json
// @Produce  json
// @Param language body services.LanguageUpdateModel  true  "Language Data"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /languages [put]
func (r *languageHandler) Update(c *gin.Context) {

	var language services.LanguageUpdateModel

	if err := c.BindJSON(&language); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	updatedLanguage, err := r.languageService.Update(c, &language)
	if err != nil {
		res := ResponseModel{
			Message: fmt.Sprint("error on update:", err.Error()),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	res := ResponseModel{
		Data: updatedLanguage,
	}
	c.JSON(http.StatusOK, res)
}

// DeleteLanguage godoc
// @ID delete-language
// @Summary Delete Language
// @Description  This endpoint serves for deleting a language
// @Tags	languages
// @Accept  json
// @Produce  json
// @Param        code   path      string  true  "Language Code"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /languages  [delete]
func (r *languageHandler) Delete(c *gin.Context) {
	code := c.Param("code")
	if err := r.languageService.Delete(c, code); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	c.Status(http.StatusOK)
}
