package httphandlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram-notification/internal/services"
	uuid "github.com/satori/go.uuid"

	"github.com/gin-gonic/gin"
)

type SmsTemplateHandler interface {
	GetByNotificationId(c *gin.Context)
	Add(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
}

type smsTemplateHandler struct {
	smsTemplateService services.SmsTemplateService
}

func NewSmsTemplateHandler(smsTemplateService services.SmsTemplateService) SmsTemplateHandler {
	return &smsTemplateHandler{smsTemplateService: smsTemplateService}
}

// GetSmsTemplateByNotificationId godoc
// @ID get-sms-template-by-notification-id
// @Summary Get SmsTemplate
// @Description  This endpoint serves for getting an smsTemplate
// @Tags	sms-templates
// @Accept  json
// @Produce  json
// @Param        notificationTypeId   path      string  true  "NotificationType ID"
// @Success      200  {object}  ResponseModel{data=services.SmsTemplateQueryModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /sms-templates/{notificationTypeId} [get]
func (r *smsTemplateHandler) GetByNotificationId(c *gin.Context) {
	notificationTypeId, _ := uuid.FromString(c.Param("notificationTypeId"))
	smsTemplate, err := r.smsTemplateService.GetByNotificationTypeId(c, notificationTypeId)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: smsTemplate,
	}
	c.JSON(http.StatusOK, res)
}

// AddSmsTemplate godoc
// @ID add-sms-template
// @Summary Add SmsTemplate
// @Description  This endpoint serves for adding a smsTemplate
// @Tags	sms-templates
// @Accept  json
// @Produce  json
// @Param   smsTemplate  body   services.SmsTemplateAddModel  true  "Request"
// @Success      200  {object}  ResponseModel{data=services.SmsTemplateGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /sms-templates [post]
func (r *smsTemplateHandler) Add(c *gin.Context) {
	var newSmsTemplate services.SmsTemplateAddModel
	if err := c.ShouldBind(&newSmsTemplate); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	smsTemplate, err := r.smsTemplateService.Add(c, &newSmsTemplate)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	res := ResponseModel{
		Data: smsTemplate,
	}
	c.JSON(http.StatusOK, res)
}

// UpdateSmsTemplate godoc
// @ID update-sms-template
// @Summary Update SmsTemplate
// @Description  This endpoint serves for updating a smsTemplate
// @Tags	sms-templates
// @Accept  json
// @Produce  json
// @Param   smsTemplate  body   services.SmsTemplateUpdateModel  true  "Request"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /sms-templates [put]
func (r *smsTemplateHandler) Update(c *gin.Context) {
	var smsTemplate services.SmsTemplateUpdateModel
	if err := c.ShouldBind(&smsTemplate); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	err := r.smsTemplateService.Update(c, &smsTemplate)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	c.Status(http.StatusOK)
}

// DeleteSmsTemplate godoc
// @ID delete-sms-template
// @Summary Delete SmsTemplate
// @Description  This endpoint serves for deleting a smsTemplate
// @Tags	sms-templates
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "ID"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /sms-templates/{id}  [delete]
func (r *smsTemplateHandler) Delete(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))
	if err := r.smsTemplateService.Delete(c, id); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	c.Status(http.StatusOK)
}
