package httphandlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram-notification/internal/services"
	uuid "github.com/satori/go.uuid"

	"github.com/gin-gonic/gin"
)

type EmailTemplateHandler interface {
	GetByNotificationId(c *gin.Context)
	Add(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
}

type emailTemplateHandler struct {
	emailTemplateService services.EmailTemplateService
}

func NewEmailTemplateHandler(emailTemplateService services.EmailTemplateService) EmailTemplateHandler {
	return &emailTemplateHandler{emailTemplateService: emailTemplateService}
}

// GetEmailTemplateByNotificationId godoc
// @ID get-email-template-by-notification-id
// @Summary Get EmailTemplate
// @Description  This endpoint serves for getting an emailTemplate
// @Tags	email-templates
// @Accept  json
// @Produce  json
// @Param        notificationTypeId   path      string  true  "NotificationType ID"
// @Success      200  {object}  ResponseModel{data=services.EmailTemplateGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /email-templates/{notificationTypeId} [get]
func (r *emailTemplateHandler) GetByNotificationId(c *gin.Context) {
	notificationTypeId, _ := uuid.FromString(c.Param("notificationTypeId"))
	emailTemplate, err := r.emailTemplateService.GetByNotificationId(c, notificationTypeId)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: emailTemplate,
	}
	c.JSON(http.StatusOK, res)
}

// AddEmailTemplate godoc
// @ID add-email-template
// @Summary Add EmailTemplate
// @Description  This endpoint serves for adding a emailTemplate
// @Tags	email-templates
// @Accept  json
// @Produce  json
// @Param   emailTemplate  formData   services.EmailTemplateAddModel  true  "Request"
// @Param   file  formData   file  true  "Request"
// @Success      200  {object}  ResponseModel{data=services.EmailTemplateGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /email-templates [post]
func (r *emailTemplateHandler) Add(c *gin.Context) {
	var newEmailTemplate services.EmailTemplateAddModel
	if err := c.ShouldBind(&newEmailTemplate); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	formFile, err := c.FormFile("file")
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	newEmailTemplate.File = formFile
	emailTemplate, err := r.emailTemplateService.Add(c, &newEmailTemplate)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	res := ResponseModel{
		Data: emailTemplate,
	}
	c.JSON(http.StatusOK, res)
}

// UpdateEmailTemplate godoc
// @ID update-email-template
// @Summary Update EmailTemplate
// @Description  This endpoint serves for updating a emailTemplate
// @Tags	email-templates
// @Accept  json
// @Produce  json
// @Param   emailTemplate  formData   services.EmailTemplateUpdateModel  true  "Request"
// @Param   file  formData   file  true  "Request"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /email-templates [put]
func (r *emailTemplateHandler) Update(c *gin.Context) {
	var emailTemplate services.EmailTemplateUpdateModel
	if err := c.ShouldBind(&emailTemplate); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	formFile, err := c.FormFile("file")
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	emailTemplate.File = formFile
	updatedEmailTemplate, err := r.emailTemplateService.Update(c, &emailTemplate)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	res := ResponseModel{
		Data: updatedEmailTemplate,
	}
	c.JSON(http.StatusOK, res)
}

// DeleteEmailTemplate godoc
// @ID delete-email-template
// @Summary Delete EmailTemplate
// @Description  This endpoint serves for deleting a emailTemplate
// @Tags	email-templates
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "ID"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /email-templates/{id}  [delete]
func (r *emailTemplateHandler) Delete(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))
	if err := r.emailTemplateService.Delete(c, id); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	c.Status(http.StatusOK)
}
