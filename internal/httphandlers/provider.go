package httphandlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram-notification/internal/services"

	"github.com/gin-gonic/gin"
)

type ProviderHandler interface {
	Get(c *gin.Context)
}

type providerHandler struct {
	providerService services.ProviderService
}

func NewProviderHandler(providerService services.ProviderService) ProviderHandler {
	return &providerHandler{providerService: providerService}
}

// GetProviders godoc
// @ID get-providers
// @Summary Get Providers
// @Description  This endpoint serves for getting all the providers
// @Tags	providers
// @Accept  json
// @Produce  json
// @Success      200  {object}  ResponseModel{data=[]services.ProviderGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /providers [get]
func (r *providerHandler) Get(c *gin.Context) {

	provider, err := r.providerService.Get(c)

	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}
	res := ResponseModel{
		Data: provider,
	}
	c.JSON(http.StatusOK, res)
}
