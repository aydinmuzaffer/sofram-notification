package httphandlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram-notification/internal/services"

	"github.com/gin-gonic/gin"
)

type RecipientTypeHandler interface {
	Get(c *gin.Context)
}

type recipientTypeHandler struct {
	recipientTypeService services.RecipientTypeService
}

func NewRecipientTypeHandler(recipientTypeService services.RecipientTypeService) RecipientTypeHandler {
	return &recipientTypeHandler{recipientTypeService: recipientTypeService}
}

// GetRecipientTypes godoc
// @ID get-recipient-types
// @Summary Get RecipientTypes
// @Description  This endpoint serves for getting all the recipientTypes
// @Tags	recipient-types
// @Accept  json
// @Produce  json
// @Success      200  {object}  ResponseModel{data=[]services.RecipientTypeGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /recipient-types [get]
func (r *recipientTypeHandler) Get(c *gin.Context) {

	recipientTypes, err := r.recipientTypeService.Get(c)

	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
	}
	res := ResponseModel{
		Data: recipientTypes,
	}
	c.JSON(http.StatusOK, res)
}
