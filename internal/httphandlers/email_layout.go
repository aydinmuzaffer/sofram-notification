package httphandlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram-notification/internal/services"
	uuid "github.com/satori/go.uuid"

	"github.com/gin-gonic/gin"
)

type EmailLayoutHandler interface {
	Get(c *gin.Context)
	GetById(c *gin.Context)
	Add(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
}

type emailLayoutHandler struct {
	emailLayoutService services.EmailLayoutService
}

func NewEmailLayoutHandler(emailLayoutService services.EmailLayoutService) EmailLayoutHandler {
	return &emailLayoutHandler{emailLayoutService: emailLayoutService}
}

// GetEmailLayouts godoc
// @ID get-email-layouts
// @Summary Get EmailLayouts
// @Description  This endpoint serves for getting all the email-layouts
// @Tags	email-layouts
// @Accept  json
// @Produce  json
// @Param        name   query      string  false  "EmailLayout Name"
// @Success      200  {object}  ResponseModel{data=[]services.EmailLayoutGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /email-layouts [get]
func (r *emailLayoutHandler) Get(c *gin.Context) {

	var emailLayouts []*services.EmailLayoutGetModel
	var err error
	name := c.Query("name")
	emailLayouts, err = r.emailLayoutService.Get(c, name)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: emailLayouts,
	}
	c.JSON(http.StatusOK, res)
}

// GetEmailLayoutById godoc
// @ID get-emailLayout
// @Summary Get EmailLayout
// @Description  This endpoint serves for getting an emailLayout
// @Tags	email-layouts
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "EmailLayout ID"
// @Success      200  {object}  ResponseModel{data=services.EmailLayoutGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /email-layouts/{id} [get]
func (r *emailLayoutHandler) GetById(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))
	emailLayout, err := r.emailLayoutService.GetById(c, id)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: emailLayout,
	}
	c.JSON(http.StatusOK, res)
}

// AddEmailLayout godoc
// @ID add-emailLayout
// @Summary Add EmailLayout
// @Description  This endpoint serves for adding a emailLayout
// @Tags	email-layouts
// @Accept  json
// @Produce  json
// @Param   emailLayout  formData   services.EmailLayoutAddModel  true  "Request"
// @Param   file  formData   file  true  "Request"
// @Success      200  {object}  ResponseModel{data=services.EmailLayoutGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /email-layouts [post]
func (r *emailLayoutHandler) Add(c *gin.Context) {

	var newEmailLayout services.EmailLayoutAddModel
	if err := c.ShouldBind(&newEmailLayout); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	formFile, err := c.FormFile("file")
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	newEmailLayout.File = formFile
	emailLayout, err := r.emailLayoutService.Add(c, &newEmailLayout)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	res := ResponseModel{
		Data: emailLayout,
	}
	c.JSON(http.StatusOK, res)
}

// UpdateEmailLayout godoc
// @ID update-emailLayout
// @Summary Update EmailLayout
// @Description  This endpoint serves for updating a emailLayout
// @Tags	email-layouts
// @Accept  json
// @Produce  json
// @Param   emailLayout  formData   services.EmailLayoutUpdateModel  true  "Request"
// @Param   file  formData   file  true  "Request"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /email-layouts [put]
func (r *emailLayoutHandler) Update(c *gin.Context) {

	var emailLayout services.EmailLayoutUpdateModel

	if err := c.ShouldBind(&emailLayout); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	formFile, err := c.FormFile("file")
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	emailLayout.File = formFile
	updatedEmailLayout, err := r.emailLayoutService.Update(c, &emailLayout)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	res := ResponseModel{
		Data: updatedEmailLayout,
	}
	c.JSON(http.StatusOK, res)
}

// DeleteEmailLayout godoc
// @ID delete-emailLayout
// @Summary Delete EmailLayout
// @Description  This endpoint serves for deleting a emailLayout
// @Tags	email-layouts
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "EmailLayout ID"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /email-layouts/{id}  [delete]
func (r *emailLayoutHandler) Delete(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))

	if err := r.emailLayoutService.Delete(c, id); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	c.Status(http.StatusOK)
}
