package httphandlers

import (
	"fmt"
	"net/http"

	"github.com/aydinmuzaffer/sofram-notification/internal/services"
	uuid "github.com/satori/go.uuid"

	"github.com/gin-gonic/gin"
)

type CategoryHandler interface {
	Get(c *gin.Context)
	GetById(c *gin.Context)
	Add(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
}

type categoryHandler struct {
	categoryService services.CategoryService
}

func NewCategoryHandler(categoryService services.CategoryService) CategoryHandler {
	return &categoryHandler{categoryService: categoryService}
}

// GetCategories godoc
// @ID get-categories
// @Summary Get Categories
// @Description  This endpoint serves for getting all the categories
// @Tags	categories
// @Accept  json
// @Produce  json
// @Success      200  {object}  ResponseModel{data=[]services.CategoryGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /categories [get]
func (r *categoryHandler) Get(c *gin.Context) {

	categories, err := r.categoryService.Get(c)

	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: categories,
	}
	c.JSON(http.StatusOK, res)
}

// GetCategor godoc
// @ID get-category
// @Summary Get Category
// @Description  This endpoint serves for getting a category
// @Tags	categories
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "Category ID"
// @Success      200  {object}  ResponseModel{data=services.CategoryGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /categories/{id} [get]
func (r *categoryHandler) GetById(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))
	category, err := r.categoryService.GetById(c, id)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: category,
	}
	c.JSON(http.StatusOK, res)
}

// AddCategory godoc
// @ID add-category
// @Summary Add Category
// @Description  This endpoint serves for adding a category
// @Tags	categories
// @Accept  json
// @Produce  json
// @Param category body services.CategoryAddModel  true  "Category Data"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /categories [post]
func (r *categoryHandler) Add(c *gin.Context) {

	var newCategory services.CategoryAddModel

	if err := c.BindJSON(&newCategory); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	if err := r.categoryService.Add(c, &newCategory); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	c.Status(http.StatusOK)
}

// UpdateCategory godoc
// @ID update-category
// @Summary Add Category
// @Description  This endpoint serves for updating a category
// @Tags	categories
// @Accept  json
// @Produce  json
// @Param category body services.CategoryUpdateModel  true  "Category Data"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /categories [put]
func (r *categoryHandler) Update(c *gin.Context) {

	var category services.CategoryUpdateModel

	if err := c.BindJSON(&category); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	if err := r.categoryService.Update(c, &category); err != nil {
		res := ResponseModel{
			Message: fmt.Sprint("error on update:", err.Error()),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	c.Status(http.StatusOK)
}

// DeleteCategory godoc
// @ID delete-category
// @Summary Delete Category
// @Description  This endpoint serves for deleting a category
// @Tags	categories
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "Category ID"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /categories/{id}  [delete]
func (r *categoryHandler) Delete(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))

	if err := r.categoryService.Delete(c, id); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	c.Status(http.StatusOK)
}
