package httphandlers

import (
	"net/http"

	"github.com/aydinmuzaffer/sofram-notification/internal/services"
	uuid "github.com/satori/go.uuid"

	"github.com/gin-gonic/gin"
)

type NotificationTypeHandler interface {
	Get(c *gin.Context)
	GetById(c *gin.Context)
	Add(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
}

type notificationTypeHandler struct {
	notificationTypeService services.NotificationTypeService
}

func NewNotificationTypeHandler(notificationTypeService services.NotificationTypeService) NotificationTypeHandler {
	return &notificationTypeHandler{notificationTypeService: notificationTypeService}
}

// GetNotificationTypes godoc
// @ID get-notification-types
// @Summary Get NotificationTypes
// @Description  This endpoint serves for getting all the notification-types
// @Tags	notification-types
// @Accept  json
// @Produce  json
// @Param        name   query      string  false  "NotificationType Name"
// @Success      200  {object}  ResponseModel{data=[]services.NotificationTypeGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /notification-types [get]
func (r *notificationTypeHandler) Get(c *gin.Context) {

	var notificationTypes []*services.NotificationTypeGetModel
	var err error
	name := c.Query("name")
	notificationTypes, err = r.notificationTypeService.Get(c, name)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: notificationTypes,
	}
	c.JSON(http.StatusOK, res)
}

// GetNotificationTypeById godoc
// @ID get-notification-type
// @Summary Get NotificationType
// @Description  This endpoint serves for getting an notificationType
// @Tags	notification-types
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "NotificationType ID"
// @Success      200  {object}  ResponseModel{data=services.NotificationTypeGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /notification-types/{id} [get]
func (r *notificationTypeHandler) GetById(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))
	notificationType, err := r.notificationTypeService.GetById(c, id)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}
	res := ResponseModel{
		Data: notificationType,
	}
	c.JSON(http.StatusOK, res)
}

// AddNotificationType godoc
// @ID add-notification-type
// @Summary Add NotificationType
// @Description  This endpoint serves for adding a notificationType
// @Tags	notification-types
// @Accept  json
// @Produce  json
// @Param   notificationType  body   services.NotificationTypeAddModel  true  "Request"
// @Success      200  {object}  ResponseModel{data=services.NotificationTypeGetModel}
// @Failure 	 422  {object}  ResponseModel
// @Router /notification-types [post]
func (r *notificationTypeHandler) Add(c *gin.Context) {

	var newNotificationType services.NotificationTypeAddModel
	if err := c.ShouldBind(&newNotificationType); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	notificationType, err := r.notificationTypeService.Add(c, &newNotificationType)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	res := ResponseModel{
		Data: notificationType,
	}
	c.JSON(http.StatusOK, res)
}

// UpdateNotificationType godoc
// @ID update-notification-type
// @Summary Update NotificationType
// @Description  This endpoint serves for updating a notificationType
// @Tags	notification-types
// @Accept  json
// @Produce  json
// @Param   notificationType  body   services.NotificationTypeUpdateModel  true  "Request"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /notification-types [put]
func (r *notificationTypeHandler) Update(c *gin.Context) {

	var notificationType services.NotificationTypeUpdateModel

	if err := c.ShouldBind(&notificationType); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	updatedNotificationType, err := r.notificationTypeService.Update(c, &notificationType)
	if err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	res := ResponseModel{
		Data: updatedNotificationType,
	}
	c.JSON(http.StatusOK, res)
}

// DeleteNotificationType godoc
// @ID delete-notification-type
// @Summary Delete NotificationType
// @Description  This endpoint serves for deleting a notificationType
// @Tags	notification-types
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "NotificationType ID"
// @Success      200
// @Failure 	 422  {object}  ResponseModel
// @Router /notification-types/{id}  [delete]
func (r *notificationTypeHandler) Delete(c *gin.Context) {
	id, _ := uuid.FromString(c.Param("id"))

	if err := r.notificationTypeService.Delete(c, id); err != nil {
		res := ResponseModel{
			Message: err.Error(),
		}
		c.JSON(http.StatusUnprocessableEntity, res)
		return
	}

	c.Status(http.StatusOK)
}
