package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type emailTemplateRepository struct {
	Db *gorm.DB
}

func NewEmailTemplateRepository(db *gorm.DB) domain.EmailTemplateRepository {
	return &emailTemplateRepository{
		Db: db,
	}
}

func (r *emailTemplateRepository) GetById(ctx context.Context, id uuid.UUID) (*domain.EmailTemplate, error) {
	emailTemplate := &domain.EmailTemplate{}
	if err := r.Db.Where("id = ?", id).Find(&emailTemplate).Error; err != nil {
		return nil, err
	}
	return emailTemplate, nil
}

func (r *emailTemplateRepository) GetByNotificationId(ctx context.Context, notificationTypeId uuid.UUID) (*domain.EmailTemplateQueryModel, error) {
	// Query with joins
	rows, err := r.Db.Table("template").Where("template.notification_type_id = ?", notificationTypeId).
		Joins("join email_template on email_template.template_id = template.id").
		Joins("join notification_type on notification_type.id = template.notification_type_id").
		Joins("join provider on provider.id = email_template.provider_id").
		Joins("join language on language.id = email_template.language_id").
		Joins("join email_layout on email_layout.id = email_template.email_layout_id").
		Select("email_template.id," +
			"email_template.template_id," +
			"email_template.email_layout_id," +
			"email_template.provider_id," +
			"email_template.language_id," +
			"email_template.bucket_key," +
			"email_template.location," +
			"email_template.subject," +
			"email_layout.description layoutDescription," +
			"email_layout.location layoutLocation," +
			"language.code langCode," +
			"provider.name provider," +
			"template.name templateName," +
			"template.notification_type_id," +
			"notification_type.name notificationType").Rows()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Values to load into
	emailTemplate := domain.EmailTemplateQueryModel{}
	for rows.Next() {
		err = rows.Scan(&emailTemplate.ID, &emailTemplate.TemplateId, &emailTemplate.EmailLayoutId, &emailTemplate.ProviderId, &emailTemplate.LanguageId,
			&emailTemplate.BucketKey, &emailTemplate.Location, &emailTemplate.Subject,
			&emailTemplate.EmailLayoutDescription, &emailTemplate.EmailLayoutLocation,
			&emailTemplate.LanguageCode,
			&emailTemplate.ProviderName,
			&emailTemplate.TemplateName,
			&emailTemplate.NotificationTypeId,
			&emailTemplate.NotificationType)
		if err != nil {
			return nil, err
		}
	}
	return &emailTemplate, nil
}

func (r *emailTemplateRepository) Save(ctx context.Context, emailTemplate *domain.EmailTemplate) error {
	return r.Db.Save(emailTemplate).Error
}
func (r *emailTemplateRepository) Delete(ctx context.Context, emailTemplate *domain.EmailTemplate) error {
	return r.Db.Delete(emailTemplate).Error
}
