package pgtest

import (
	"context"
	"database/sql"
	"regexp"
	"time"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	"github.com/aydinmuzaffer/sofram-notification/internal/repos"

	"github.com/DATA-DOG/go-sqlmock"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	uuid "github.com/satori/go.uuid"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var _ = Describe("Repository", func() {
	var repository domain.CategoryRepository
	var mock sqlmock.Sqlmock

	BeforeEach(func() {
		var db *sql.DB
		var err error

		// db, mock, err = sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual)) // use equal matcher
		db, mock, err = sqlmock.New() // mock sql.DB
		Expect(err).ShouldNot(HaveOccurred())

		gdb, err := gorm.Open(postgres.New(postgres.Config{
			Conn: db,
		}), &gorm.Config{}) // open gorm db
		Expect(err).ShouldNot(HaveOccurred())

		repository = repos.NewCategoryRepository(gdb)
	})
	AfterEach(func() {
		err := mock.ExpectationsWereMet() // make sure all expectations were met
		Expect(err).ShouldNot(HaveOccurred())
	})

	Context("get all", func() {
		It("empty", func() {
			const sqlSelectAll = `SELECT * FROM "category"`
			mock.ExpectQuery(regexp.QuoteMeta(sqlSelectAll)).
				WillReturnRows(sqlmock.NewRows(nil))

			l, err := repository.Get(context.TODO())
			Expect(err).ShouldNot(HaveOccurred())
			Expect(l).Should(BeEmpty())
		})
	})

	Context("get-by-id", func() {
		It("found", func() {
			category := &domain.Category{
				BaseEntitiy: domain.BaseEntitiy{
					ID:        uuid.NewV4(),
					CreatedAt: time.Now(),
					UpdatedAt: time.Now(),
				},
				Name:        "test-name",
				Description: "test-description",
			}

			rows := sqlmock.
				NewRows([]string{"id", "name", "description", "created_at", "updated_at", "deleted_at"}).
				AddRow(category.ID, category.Name, category.Description, category.CreatedAt, category.UpdatedAt, category.DeletedAt)

			const sqlSelectOne = `SELECT * FROM "category" WHERE id = $1 ORDER BY "category"."id" LIMIT 1`

			mock.ExpectQuery(regexp.QuoteMeta(sqlSelectOne)).
				WithArgs(category.ID).
				WillReturnRows(rows)

			dbBlog, err := repository.GetById(context.TODO(), category.ID)
			Expect(err).ShouldNot(HaveOccurred())
			Expect(dbBlog).Should(Equal(category))
		})

		It("not found", func() {
			// ignore sql match
			mock.ExpectQuery(`.+`).WillReturnRows(sqlmock.NewRows(nil))
			_, err := repository.GetById(context.TODO(), uuid.NewV4())
			Expect(err).Should(Equal(gorm.ErrRecordNotFound))
		})
	})

	// Context("list", func() {
	// 	It("found", func() {
	// 		rows := sqlmock.
	// 			NewRows([]string{"id", "name", "description", "created_at", "updated_at", "deleted_at"}).
	// 			AddRow(uuid.NewV4().String(), "test-name-1", "test-description-1", time.Now(), nil, nil).
	// 			AddRow(uuid.NewV4().String(), "test-name-2", "test-description-2", time.Now(), nil, nil)

	// 		// limit/offset is not parameter
	// 		const sqlSelectFirstTen = `SELECT * FROM "blogs" LIMIT 10 OFFSET 0`
	// 		mock.ExpectQuery(regexp.QuoteMeta(sqlSelectFirstTen)).WillReturnRows(rows)

	// 		l, err := repository.List(0, 10)
	// 		Expect(err).ShouldNot(HaveOccurred())

	// 		Expect(l).Should(HaveLen(2))
	// 		Expect(l[0].Tags).Should(BeEmpty())
	// 		Expect(l[1].Tags).Should(Equal(pq.StringArray{"go"}))
	// 		Expect(l[1].ID).Should(BeEquivalentTo(2)) // use BeEquivalentTo
	// 	})
	// 	It("not found", func() {
	// 		// ignore sql match
	// 		mock.ExpectQuery(`.+`).WillReturnRows(sqlmock.NewRows(nil))
	// 		l, err := repository.List(0, 10)
	// 		Expect(err).ShouldNot(HaveOccurred())
	// 		Expect(l).Should(BeEmpty())
	// 	})
	// })

	Context("save", func() {
		var category *domain.Category
		BeforeEach(func() {
			category = &domain.Category{
				BaseEntitiy: domain.BaseEntitiy{
					CreatedAt: time.Now(),
					UpdatedAt: time.Now(),
				},
				Name:        "test-name",
				Description: "test-description",
			}
		})

		It("update", func() {
			const sqlUpdate = `
			UPDATE "category" SET "name"=$1, "description"=$2 WHERE "id" = $3`
			const sqlSelectOne = `
					SELECT * FROM "category" WHERE "category"."id" = $1 ORDER BY "category"."id" ASC LIMIT 1`

			category.ID = uuid.NewV4()

			mock.ExpectBegin()
			mock.ExpectExec(regexp.QuoteMeta(sqlUpdate)).
				WithArgs(category.Name, category.Description, category.ID).
				WillReturnResult(sqlmock.NewResult(0, 0))
			mock.ExpectCommit()

			// select after update
			mock.ExpectQuery(regexp.QuoteMeta(sqlSelectOne)).
				WithArgs(category.ID).
				WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(category.ID))

			err := repository.Save(context.TODO(), category)
			Expect(err).ShouldNot(HaveOccurred())
		})

		It("insert", func() {
			// gorm use query instead of exec
			// https://github.com/DATA-DOG/go-sqlmock/issues/118
			id := uuid.NewV4()
			name := "test-name"
			desciption := "test-description"
			// createdAt := time.Now()
			// updatedAt := time.Now()
			const sqlInsert = `
			INSERT INTO "category" ("id","created_at","updated_at","deleted_at","name","description") VALUES ($1,$2,$3,$4,$5,$6) RETURNING "id","created_at","updated_at"`
			newId := uuid.NewV4().String()
			mock.ExpectBegin() // start transaction
			// mock.ExpectExec(regexp.QuoteMeta(sqlInsert)).
			// 	WithArgs(id, createdAt, updatedAt, nil, name, desciption).
			// 	WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at"}).
			// 		AddRow(id, createdAt, updatedAt))
			mock.ExpectExec(regexp.QuoteMeta(sqlInsert)).
				WithArgs(id, nil, nil, nil, nil, nil).WillReturnResult(sqlmock.NewResult(0, 1))

			mock.ExpectCommit() // commit transaction

			//Expect(category.ID).Should(BeEmpty())
			category = &domain.Category{
				Name:        name,
				Description: desciption,
			}
			err := repository.Save(context.TODO(), category)
			Expect(err).ShouldNot(HaveOccurred())

			Expect(category.ID).Should(BeEquivalentTo(newId))

		})
	})

	// Context("search by title", func() {
	// 	It("found", func() {
	// 		rows := sqlmock.
	// 			NewRows([]string{"id", "title", "content", "tags", "created_at"}).
	// 			AddRow(1, "post 1", "hello 1", nil, time.Now())

	// 		// limit/offset is not parameter
	// 		const sqlSearch = `
	// 			SELECT * FROM "blogs"
	// 			WHERE (title like $1)
	// 			LIMIT 10 OFFSET 0`
	// 		const q = "os"

	// 		mock.ExpectQuery(regexp.QuoteMeta(sqlSearch)).
	// 			WithArgs("%" + q + "%").
	// 			WillReturnRows(rows)

	// 		l, err := repository.SearchByTitle(q, 0, 10)
	// 		Expect(err).ShouldNot(HaveOccurred())

	// 		Expect(l).Should(HaveLen(1))
	// 		Expect(l[0].Title).Should(ContainSubstring(q))
	// 	})
	// })
})
