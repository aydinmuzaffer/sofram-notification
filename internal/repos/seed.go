package repos

import (
	"errors"
	"fmt"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	"github.com/golang-migrate/migrate/v4"
	postgresMigrate "github.com/golang-migrate/migrate/v4/database/postgres"
	"gorm.io/gorm"
)

type SeedRepository interface {
	MigrateDB() error
	SeedDB() error
}

type seedRepository struct {
	Db *gorm.DB
}

func NewSeedRepository(db *gorm.DB) SeedRepository {
	return &seedRepository{
		Db: db,
	}
}

// MigrateDB - runs all migrations in the migrations
func (sr *seedRepository) MigrateDB() error {

	db, err := sr.Db.DB()
	if err != nil {
		return err
	}

	driver, err := postgresMigrate.WithInstance(db, &postgresMigrate.Config{})
	if err != nil {
		return fmt.Errorf("could not create the postgres driver: %w", err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file:///migrations",
		"postgres",
		driver,
	)
	if err != nil {
		return err
	}

	if err := m.Up(); err != nil {
		if !errors.Is(err, migrate.ErrNoChange) {
			return fmt.Errorf("could not run up migrations: %w", err)
		}
	}

	return nil
}
func (sr *seedRepository) SeedDB() error {

	err := sr.Db.Debug().Transaction(func(tx *gorm.DB) error {
		// do some database operations in the transaction (use 'tx' from this point, not 'db')
		if err := checkDefaultChanels(tx); err != nil {
			return err
		}

		if err := checkDefaultProviders(tx); err != nil {
			return err
		}

		if err := checkDefaultRecipientTypes(tx); err != nil {
			return err
		}
		// return nil will commit the whole transaction
		return nil
	})

	if err != nil {
		return err
	}

	return nil
}

func checkDefaultChanels(tx *gorm.DB) error {
	mail := domain.ChanelEnum.Mail
	if err := setChanel(tx, mail); err != nil {
		return err
	}

	sms := domain.ChanelEnum.Sms
	if err := setChanel(tx, sms); err != nil {
		return err
	}

	return nil
}

func setChanel(tx *gorm.DB, r *domain.Chanel) error {
	exists, err := rowExists(tx, "SELECT 1 FROM chanel WHERE name = ?", r.Name)
	if err != nil {
		return err
	}
	if !exists {
		if err := createRow(tx, r); err != nil {
			return err
		}
		return nil
	} else {
		existing := &domain.Chanel{}
		tx.Where("name = ?", r.Name).First(existing)
		r = existing
		return nil
	}
}

func checkDefaultProviders(tx *gorm.DB) error {
	awsSES := domain.ProviderEnum.AmazonSES
	awsSES.ChanelId = domain.ChanelEnum.Mail.ID
	if err := setProvider(tx, awsSES); err != nil {
		return err
	}

	gmail := domain.ProviderEnum.Gmail
	gmail.ChanelId = domain.ChanelEnum.Mail.ID
	if err := setProvider(tx, gmail); err != nil {
		return err
	}

	twilloSms := domain.ProviderEnum.TwilloSMS
	twilloSms.ChanelId = domain.ChanelEnum.Sms.ID
	if err := setProvider(tx, twilloSms); err != nil {
		return err
	}

	return nil
}

func setProvider(tx *gorm.DB, r *domain.Provider) error {
	exists, err := rowExists(tx, "SELECT 1 FROM provider WHERE name = ?", r.Name)
	if err != nil {
		return err
	}

	if !exists {
		if err := createRow(tx, r); err != nil {
			return err
		}
		return nil
	} else {
		existing := &domain.Provider{}
		tx.Where("name = ?", r.Name).First(existing)
		r = existing
		return nil
	}
}

func checkDefaultRecipientTypes(tx *gorm.DB) error {
	to := domain.RecipientTypeEnum.To
	if err := setRecipientType(tx, to); err != nil {
		return err
	}

	from := domain.RecipientTypeEnum.From
	if err := setRecipientType(tx, from); err != nil {
		return err
	}

	cc := domain.RecipientTypeEnum.Cc
	if err := setRecipientType(tx, cc); err != nil {
		return err
	}

	bcc := domain.RecipientTypeEnum.Bcc
	if err := setRecipientType(tx, bcc); err != nil {
		return err
	}

	return nil
}

func setRecipientType(tx *gorm.DB, r *domain.RecipientType) error {
	exists, err := rowExists(tx, "SELECT 1 FROM recipient_type WHERE name = ?", r.Name)
	if err != nil {
		return err
	}

	if !exists {
		if err := createRow(tx, r); err != nil {
			return err
		}
		return nil
	} else {
		existing := &domain.RecipientType{}
		tx.Where("name = ?", r.Name).First(existing)
		r = existing
		return nil
	}
}

func rowExists(tx *gorm.DB, query string, args ...interface{}) (bool, error) {
	var exists bool
	query = fmt.Sprintf("SELECT exists (%s)", query)
	err := tx.Raw(query, args...).Row().Scan(&exists)
	if err != nil {
		return false, fmt.Errorf("error checking if row exists '%s' %v", args, err.Error())
	}
	return exists, nil
}

func createRow(tx *gorm.DB, value interface{}) error {
	if err := tx.Session(&gorm.Session{SkipHooks: true}).Debug().Create(value).Error; err != nil {
		return err
	}
	return nil
}
