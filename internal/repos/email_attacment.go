package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type emailAttachmentRepository struct {
	Db *gorm.DB
}

func NewEmailAttachmentRepository(db *gorm.DB) domain.EmailAttachmentRepository {
	return &emailAttachmentRepository{
		Db: db,
	}
}

func (r *emailAttachmentRepository) Get(ctx context.Context, notificationId uuid.UUID) ([]*domain.EmailAttachment, error) {
	emaiAttachments := []*domain.EmailAttachment{}
	err := r.Db.Where("notification_id = ?", notificationId).Find(&emaiAttachments).Error
	return emaiAttachments, err

}

func (r *emailAttachmentRepository) Save(ctx context.Context, emailAttachment *domain.EmailAttachment) error {
	return r.Db.Save(emailAttachment).Error
}
