package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type smsTemplateRepository struct {
	Db *gorm.DB
}

func NewSmsTemplateRepository(db *gorm.DB) domain.SmsTemplateRepository {
	return &smsTemplateRepository{
		Db: db,
	}
}

func (r *smsTemplateRepository) Get(ctx context.Context) ([]*domain.SmsTemplate, error) {
	smsTemplates := []*domain.SmsTemplate{}

	if err := r.Db.Find(smsTemplates).Error; err != nil {
		return nil, err
	}

	return smsTemplates, nil

}
func (r *smsTemplateRepository) GetById(ctx context.Context, id uuid.UUID) (*domain.SmsTemplate, error) {
	smsTemplate := &domain.SmsTemplate{}
	if err := r.Db.Where("id = ?", id).First(&smsTemplate).Error; err != nil {
		return nil, err
	}
	return smsTemplate, nil
}

func (r *smsTemplateRepository) GetByNotificationTypeId(ctx context.Context, notificationTypeId uuid.UUID) (*domain.SmsTemplateQueryModel, error) {
	// Query with joins
	rows, err := r.Db.Table("template").Where("template.notification_type_id = ?", notificationTypeId).
		Joins("join sms_template on sms_template.template_id = template.id").
		Joins("join notification_type on notification_type.id = template.notification_type_id").
		Joins("join provider on provider.id = sms_template.provider_id").
		Joins("join language on language.id = sms_template.language_id").
		Select("sms_template.id," +
			"sms_template.template_id," +
			"sms_template.provider_id," +
			"sms_template.language_id," +
			"sms_template.content," +
			"language.code langCode," +
			"provider.name provider," +
			"template.name templateName," +
			"template.notification_type_id," +
			"notification_type.name notificationType").Rows()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Values to load into
	smsTemplate := domain.SmsTemplateQueryModel{}
	for rows.Next() {
		err = rows.Scan(&smsTemplate.ID, &smsTemplate.TemplateId, &smsTemplate.ProviderId, &smsTemplate.LanguageId, &smsTemplate.Content,
			&smsTemplate.LanguageCode,
			&smsTemplate.ProviderName,
			&smsTemplate.TemplateName,
			&smsTemplate.NotificationTypeId,
			&smsTemplate.NotificationType)
		if err != nil {
			return nil, err
		}
	}
	return &smsTemplate, nil
}

func (r *smsTemplateRepository) Save(ctx context.Context, smsTemplate *domain.SmsTemplate) error {
	return r.Db.Save(smsTemplate).Error
}

func (r *smsTemplateRepository) Delete(ctx context.Context, smsTemplate *domain.SmsTemplate) error {
	return r.Db.Delete(smsTemplate).Error
}
