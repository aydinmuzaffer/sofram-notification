package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	"gorm.io/gorm"
)

type transactionRecipientRepository struct {
	Db *gorm.DB
}

func NewTransactionRecipientRepository(db *gorm.DB) domain.TransactionRecipientRepository {
	return &transactionRecipientRepository{
		Db: db,
	}
}

func (r *transactionRecipientRepository) Save(ctx context.Context, transactionRecipient *domain.TransactionRecipient) error {
	return r.Db.Save(transactionRecipient).Error
}
