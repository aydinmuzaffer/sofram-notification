package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	soframerror "github.com/aydinmuzaffer/sofram-notification/internal/error"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type templateRepository struct {
	Db *gorm.DB
}

func NewTemplateRepository(db *gorm.DB) domain.TemplateRepository {
	return &templateRepository{
		Db: db,
	}
}

func (r *templateRepository) Get(ctx context.Context, name string) ([]*domain.Template, error) {
	templates := []*domain.Template{}
	if name == "" {
		if err := r.Db.Find(&templates).Error; err != nil {
			return nil, err
		}
	} else {
		if err := r.Db.Where("name LIKE ?", name).Find(&templates).Error; err != nil {
			return nil, err
		}
	}
	return templates, nil
}
func (r *templateRepository) GetById(ctx context.Context, id uuid.UUID) (*domain.TemplateQueryModel, error) {
	// Query with joins
	rows, err := r.Db.Table("template").Where("template.id = ?", id).
		Joins("join notification_type on notification_type.id = template.notification_type_id").
		Joins("left join email_template on email_template.template_id = template.id").
		Joins("left join provider ep on ep.id = email_template.provider_id").
		Joins("left join language el on el.id = email_template.language_id").
		Joins("left join email_layout on email_layout.id = email_template.email_layout_id").
		Joins("left join sms_template on sms_template.template_id = template.id").
		Joins("left join provider sp on sp.id = sms_template.provider_id").
		Joins("left join language sl on sl.id = sms_template.language_id").
		Select("COALESCE(email_template.id, uuid_nil())," +
			"COALESCE(email_template.template_id, uuid_nil())," +
			"COALESCE(email_template.email_layout_id, uuid_nil())," +
			"COALESCE(email_template.provider_id, uuid_nil())," +
			"COALESCE(email_template.language_id, uuid_nil())," +
			"email_template.bucket_key," +
			"email_template.location," +
			"email_template.subject," +
			"email_layout.description layoutDescription," +
			"email_layout.location layoutLocation," +
			"el.code emailLangCode," +
			"ep.name emailProvider," +
			"template.id templateId," +
			"template.name templateName," +
			"template.notification_type_id," +
			"notification_type.name notificationType," +
			"sms_template.content smsContent," +
			"COALESCE(sl.id, uuid_nil()) smsLangId," +
			"sl.code smsLangCode," +
			"COALESCE(ep.id, uuid_nil()) smsProviderId," +
			"ep.name smsProvider").Rows()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Values to load into
	template := &domain.TemplateQueryModel{}
	template.Sms = make([]*domain.SmsTemplateQueryModel, 0)
	template.Email = make([]*domain.EmailTemplateQueryModel, 0)
	for rows.Next() {
		emailTemplate := domain.EmailTemplateQueryModel{}
		smsTemplate := domain.SmsTemplateQueryModel{}
		err = rows.Scan(&emailTemplate.ID, &emailTemplate.TemplateId, &emailTemplate.EmailLayoutId, &emailTemplate.ProviderId, &emailTemplate.LanguageId,
			&emailTemplate.BucketKey, &emailTemplate.Location, &emailTemplate.Subject,
			&emailTemplate.EmailLayoutDescription, &emailTemplate.EmailLayoutLocation,
			&emailTemplate.LanguageCode,
			&emailTemplate.ProviderName,
			&template.Id,
			&template.Name,
			&template.NotificationTypeId,
			&template.NotificationType,
			&smsTemplate.Content,
			&smsTemplate.LanguageId,
			&smsTemplate.LanguageCode,
			&smsTemplate.ProviderId,
			&smsTemplate.ProviderName)
		if err != nil {
			return nil, err
		}

		if smsTemplate.ID != uuid.Nil {
			template.Sms = append(template.Sms, &smsTemplate)
		}
		if emailTemplate.ID != uuid.Nil {
			template.Email = append(template.Email, &emailTemplate)
		}
	}

	uniqueSms := make(map[uuid.UUID]*domain.SmsTemplateQueryModel)
	smsSlice := []*domain.SmsTemplateQueryModel{}
	for _, sms := range template.Sms {
		if _, ok := uniqueSms[sms.ID]; ok {
			continue
		} else {
			uniqueSms[sms.ID] = sms
			smsSlice = append(smsSlice, sms)
		}
	}
	uniqueEmail := make(map[uuid.UUID]*domain.EmailTemplateQueryModel)
	emailSlice := []*domain.EmailTemplateQueryModel{}
	for _, email := range template.Email {
		if _, ok := uniqueEmail[email.ID]; ok {
			continue
		} else {
			uniqueEmail[email.ID] = email
			emailSlice = append(emailSlice, email)
		}
	}
	template.Sms = smsSlice
	template.Email = emailSlice
	return template, nil
}
func (r *templateRepository) GetByNotificationTypeId(ctx context.Context, notificationTypeId uuid.UUID) (*domain.Template, error) {
	template := &domain.Template{}
	if err := r.Db.Where("notification_type_id = ?", notificationTypeId).First(template).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, errors.Wrap(soframerror.ErrRecordNotFound.Wrap(err), "[TemplateRepository.GetByNotificationTypeId] error")
		}
		return nil, err
	}
	return template, nil
}
func (r *templateRepository) Save(ctx context.Context, template *domain.Template) error {
	return r.Db.Save(template).Error
}

func (r *templateRepository) Delete(ctx context.Context, template *domain.Template) error {
	return r.Db.Delete(template).Error
}
