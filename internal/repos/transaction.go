package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	"gorm.io/gorm"
)

type transactionRepository struct {
	Db *gorm.DB
}

func NewTransactionRepository(db *gorm.DB) domain.TransactionRepository {
	return &transactionRepository{
		Db: db,
	}
}

func (r *transactionRepository) Save(ctx context.Context, transaction *domain.Transaction) error {
	return r.Db.Save(transaction).Error
}
