package repos

import (
	"context"
	"errors"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
)

type providerepository struct{}

func NewProviderRepository() domain.ProviderRepository {
	return &providerepository{}
}

func (r *providerepository) Get(ctx context.Context) ([]*domain.Provider, error) {

	providers := domain.GetEnumList[any, *domain.Provider](domain.ProviderEnum)

	if len(providers) == 0 {
		return providers, errors.New("empty providers")
	}
	return providers, nil
}
