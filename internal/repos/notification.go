package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	"gorm.io/gorm"
)

type notificationRepository struct {
	Db *gorm.DB
}

func NewNotificationRepository(db *gorm.DB) domain.NotificationRepository {
	return &notificationRepository{
		Db: db,
	}
}

func (r *notificationRepository) Save(ctx context.Context, notification *domain.Notification) error {
	return r.Db.Save(notification).Error
}
