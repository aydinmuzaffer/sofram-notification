package repos

import (
	"context"
	"errors"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
)

type chanelRepository struct{}

func NewChanelRepository() domain.ChanelRepository {
	return &chanelRepository{}
}

func (r *chanelRepository) Get(ctx context.Context) ([]*domain.Chanel, error) {

	chanels := domain.GetEnumList[any, *domain.Chanel](domain.ChanelEnum)

	if len(chanels) == 0 {
		return chanels, errors.New("empty chanels")
	}
	return chanels, nil
}
