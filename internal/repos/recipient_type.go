package repos

import (
	"context"
	"errors"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
)

type recipientTypeRepository struct{}

func NewRecipientTypeRepository() domain.RecipientTypeRepository {
	return &recipientTypeRepository{}
}

func (r *recipientTypeRepository) Get(ctx context.Context) ([]*domain.RecipientType, error) {

	recipientTypes := domain.GetEnumList[any, *domain.RecipientType](domain.RecipientTypeEnum)

	if len(recipientTypes) == 0 {
		return recipientTypes, errors.New("empty recipientTypes")
	}
	return recipientTypes, nil
}
