package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type emailLayoutRepository struct {
	Db *gorm.DB
}

func NewEmailLayoutRepository(db *gorm.DB) domain.EmailLayoutRepository {
	return &emailLayoutRepository{
		Db: db,
	}
}

func (r *emailLayoutRepository) Get(ctx context.Context, name string) ([]*domain.EmailLayout, error) {
	emailLayouts := []*domain.EmailLayout{}
	if name == "" {
		if err := r.Db.Find(&emailLayouts).Error; err != nil {
			return nil, err
		}
	} else {
		if err := r.Db.Where("name LIKE ?", name).Find(&emailLayouts).Error; err != nil {
			return nil, err
		}
	}
	return emailLayouts, nil

}
func (r *emailLayoutRepository) GetById(ctx context.Context, id uuid.UUID) (*domain.EmailLayout, error) {
	emailLayout := &domain.EmailLayout{}
	err := r.Db.First(emailLayout, "id = ?", id).Error
	return emailLayout, err
}
func (r *emailLayoutRepository) Save(ctx context.Context, emailLayout *domain.EmailLayout) error {
	return r.Db.Save(emailLayout).Error
}
func (r *emailLayoutRepository) Delete(ctx context.Context, emailLayout *domain.EmailLayout) error {
	return r.Db.Delete(emailLayout).Error
}
