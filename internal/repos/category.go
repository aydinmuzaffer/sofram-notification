package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

var _ domain.CategoryRepository = (*categoryRepository)(nil)

type categoryRepository struct {
	Db *gorm.DB
}

func NewCategoryRepository(db *gorm.DB) domain.CategoryRepository {
	return &categoryRepository{
		Db: db,
	}
}

func (r *categoryRepository) Get(ctx context.Context) ([]*domain.Category, error) {
	categories := []*domain.Category{}
	err := r.Db.Find(&categories).Error
	return categories, err

}
func (r *categoryRepository) GetById(ctx context.Context, id uuid.UUID) (*domain.Category, error) {
	category := &domain.Category{}
	err := r.Db.First(category, "id = ?", id).Error
	return category, err
}
func (r *categoryRepository) Save(ctx context.Context, categotry *domain.Category) error {
	return r.Db.Save(categotry).Error
}
func (r *categoryRepository) Delete(ctx context.Context, categotry *domain.Category) error {
	return r.Db.Delete(categotry).Error
}
