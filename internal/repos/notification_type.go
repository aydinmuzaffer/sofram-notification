package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type notificationTypeRepository struct {
	Db *gorm.DB
}

func NewNotificationTypeRepository(db *gorm.DB) domain.NotificationTypeRepository {
	return &notificationTypeRepository{
		Db: db,
	}
}

func (r *notificationTypeRepository) Get(ctx context.Context, name string) ([]*domain.NotificationType, error) {
	notificationTypes := []*domain.NotificationType{}
	if name == "" {
		if err := r.Db.Find(&notificationTypes).Error; err != nil {
			return nil, err
		}
	} else {
		if err := r.Db.Where("name LIKE ?", name).Find(&notificationTypes).Error; err != nil {
			return nil, err
		}
	}
	return notificationTypes, nil
}
func (r *notificationTypeRepository) GetById(ctx context.Context, id uuid.UUID) (*domain.NotificationType, error) {
	notificationType := &domain.NotificationType{}
	err := r.Db.First(notificationType, "id = ?", id).Error
	return notificationType, err
}
func (r *notificationTypeRepository) Save(ctx context.Context, notificationType *domain.NotificationType) error {
	return r.Db.Save(notificationType).Error
}
func (r *notificationTypeRepository) Delete(ctx context.Context, notificationType *domain.NotificationType) error {
	return r.Db.Delete(notificationType).Error
}
