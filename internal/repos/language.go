package repos

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type languageRepository struct {
	Db *gorm.DB
}

func NewLanguageRepository(db *gorm.DB) domain.LanguageRepository {
	return &languageRepository{
		Db: db,
	}
}

func (r *languageRepository) Get(ctx context.Context, code string) ([]*domain.Language, error) {
	var languages []*domain.Language
	if code == "" {
		if err := r.Db.Find(&languages).Error; err != nil {
			return nil, err
		}
	} else {
		if err := r.Db.Where("code like ?", code).Find(&languages).Error; err != nil {
			return nil, err
		}
	}
	return languages, nil

}
func (r *languageRepository) GetByCode(ctx context.Context, code string) (*domain.Language, error) {
	language := &domain.Language{}
	if err := r.Db.Where("code = ?", code).Find(&language).Error; err != nil {
		return nil, err
	}
	return language, nil
}
func (r *languageRepository) GetById(ctx context.Context, id uuid.UUID) (*domain.Language, error) {
	language := &domain.Language{}
	if err := r.Db.Where("id = ?", id).Find(&language).Error; err != nil {
		return nil, err
	}
	return language, nil
}
func (r *languageRepository) Save(ctx context.Context, language *domain.Language) error {
	return r.Db.Save(language).Error
}
func (r *languageRepository) Delete(ctx context.Context, language *domain.Language) error {
	return r.Db.Delete(language).Error
}
