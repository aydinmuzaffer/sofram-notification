package soframerror

import (
	"fmt"
)

// compile time proofs and errors.
var (
	_ error         = (SoframErrorer)(nil)
	_ SoframErrorer = (*SoframError)(nil)

	ErrRecordNotFound = &SoframError{Message: "record not found", Code: CodeRecordNotFound}
)

// SoframErrorer represents custom error interface.
type SoframErrorer interface {
	Wrap(err error) SoframErrorer
	Error() string
}

// SoframError represents the error structure.
type SoframError struct {
	Err     error
	Message string
	Code    ErrorCode
}

// New returns new SoframError.
func New(message string, code string) SoframErrorer {
	return &SoframError{
		Message: message,
		Code:    ErrorCode(code),
	}
}

// Error interface.
func (s *SoframError) Error() string {
	if s.Err != nil {
		return fmt.Sprintf("%s message: %s code: %s", s.Err.Error(), s.Message, s.Code)
	}

	return fmt.Sprintf("message: %s code: %s", s.Message, s.Code)
}

// Wrap sets inner error.
func (s *SoframError) Wrap(err error) SoframErrorer {
	scErr := s
	scErr.Err = err
	return scErr
}
