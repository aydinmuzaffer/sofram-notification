package soframerror

// ErrorCode is a string type for error codes.
type ErrorCode string

// error codes.
const (
	CodeRecordNotFound ErrorCode = "record-not-found"
)
