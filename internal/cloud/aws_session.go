package cloud

import (
	"net"
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
)

func NewAwsSession(profile, endpoint, region string) (*session.Session, error) {
	awsConf := aws.Config{
		HTTPClient: &http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   30 * time.Second,
					KeepAlive: 30 * time.Second,
				}).DialContext,
				MaxIdleConns:          380,
				MaxIdleConnsPerHost:   160,
				IdleConnTimeout:       90 * time.Second,
				TLSHandshakeTimeout:   10 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
			},
		},
	}

	if endpoint != "" {
		awsConf.Endpoint = aws.String(endpoint)
		awsConf.S3ForcePathStyle = aws.Bool(true)
		awsConf.Region = aws.String(endpoints.UsWest2RegionID)
		awsConf.Credentials = credentials.NewStaticCredentials("foo", "var", "")
	} else {
		awsConf.Credentials = credentials.NewSharedCredentials("", profile)
		awsConf.Region = aws.String(region)
	}

	opts, err := session.NewSessionWithOptions(session.Options{
		Config:            awsConf,
		SharedConfigState: session.SharedConfigEnable,
	})

	if err != nil {
		return &session.Session{}, err
	}

	sess := session.Must(opts, nil)
	return sess, nil

}
