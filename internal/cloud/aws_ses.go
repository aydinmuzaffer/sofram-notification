package cloud

import (
	"bytes"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
	"gopkg.in/gomail.v2"
)

type AwsSES interface {
	SendEmail(model EmailSendModel) (string, error)
}

type awsSES struct {
	Session *session.Session
}

func NewAwsSES(session *session.Session) AwsSES {
	return &awsSES{
		Session: session,
	}
}

func (s *awsSES) SendEmail(model EmailSendModel) (string, error) {
	msg := gomail.NewMessage()
	msg.SetAddressHeader("From", model.From.Address, model.From.Name)
	msg.SetHeader("Subject", model.Subject)
	msg.SetBody("text/html", model.Body)

	var recipients []*string

	var to []string
	for _, t := range model.To {
		to = append(to, t.Address)
		recipients = append(recipients, &t.Address)
	}
	msg.SetHeader("To", to...)

	if len(model.Cc) != 0 {
		var cc []string
		for _, c := range model.Bcc {
			cc = append(cc, c.Address)
			recipients = append(recipients, &c.Address)
		}
		msg.SetHeader("cc", cc...)
	}

	if len(model.Bcc) != 0 {
		var bcc []string
		for _, b := range model.Bcc {
			bcc = append(bcc, b.Address)
			recipients = append(recipients, &b.Address)
		}
		msg.SetHeader("bcc", bcc...)
	}

	// If attachments exists
	if len(model.Attachment) != 0 {
		for _, f := range model.Attachment {
			msg.Attach(f)
		}
	}

	// create a new buffer to add raw data
	var emailRaw bytes.Buffer
	msg.WriteTo(&emailRaw)

	// create new raw message
	message := ses.RawMessage{Data: emailRaw.Bytes()}

	input := &ses.SendRawEmailInput{Source: &model.From.Address, Destinations: recipients, RawMessage: &message}

	svc := ses.New(s.Session)
	res, err := svc.SendRawEmail(input)

	if err != nil {
		return "", err
	}
	return *res.MessageId, nil
}

type EmailSendModel struct {
	From       RecipientModel
	To         []RecipientModel
	Bcc        []RecipientModel
	Cc         []RecipientModel
	Subject    string
	Body       string
	Attachment []string
}

type RecipientModel struct {
	Address string
	Name    string
}
