package cloud

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type S3Bucket interface {
	Download(ctx context.Context, key string) (bool, []byte, error)
	Upload(ctx context.Context, key string, data []byte, contentType string) (*s3manager.UploadOutput, error)
	Delete(ctx context.Context, key string) error
	GetS3Key(folder string, key string) string
	GetS3InputName(fileName string) string
	GetUploadObject(file *multipart.FileHeader, folder string) (string, []byte, error)
}

type s3Handler struct {
	Session *session.Session
	Bucket  string
}

func NewS3Handler(sess *session.Session, bucket string) S3Bucket {
	return &s3Handler{
		Session: sess,
		Bucket:  bucket,
	}
}

func (s *s3Handler) Download(ctx context.Context, key string) (bool, []byte, error) {
	svc := s3.New(s.Session)
	_, err := svc.HeadObjectWithContext(ctx, &s3.HeadObjectInput{
		Bucket: aws.String(s.Bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return false, nil, fmt.Errorf("HeadObjectInputError: %s with Key: %s and Bucket: %s", err.Error(), key, s.Bucket)
	}
	//Download image from s3
	buffer := &aws.WriteAtBuffer{}
	downloader := s3manager.NewDownloader(s.Session)
	_, err = downloader.DownloadWithContext(ctx, buffer, &s3.GetObjectInput{
		Bucket: aws.String(s.Bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return true, nil, fmt.Errorf(fmt.Sprintf("could not download image from S3: %v", err))
	}
	return true, buffer.Bytes(), nil
}

func (s *s3Handler) Upload(ctx context.Context, key string, data []byte, contentType string) (*s3manager.UploadOutput, error) {
	uploader := s3manager.NewUploader(s.Session)
	output, err := uploader.UploadWithContext(ctx, &s3manager.UploadInput{
		Bucket:      aws.String(s.Bucket),
		Key:         aws.String(key),
		ContentType: aws.String(contentType),
		Body:        bytes.NewReader(data),
	})

	if err != nil {
		return nil, err
	}
	return output, nil
}

func (s *s3Handler) Delete(ctx context.Context, key string) error {
	svc := s3.New(s.Session)
	_, err := svc.DeleteObject(&s3.DeleteObjectInput{
		Bucket: &s.Bucket,
		Key:    &key,
	})
	if err != nil {
		return err
	}

	err = svc.WaitUntilObjectNotExists(&s3.HeadObjectInput{
		Bucket: &s.Bucket,
		Key:    &key,
	})
	if err != nil {
		return err
	}
	return nil
}

func (s *s3Handler) GetS3Key(folder string, key string) string {
	return fmt.Sprintf("%s/%s", folder, key)
}

func (s *s3Handler) GetS3InputName(fileName string) string {
	t := time.Now()
	formatted := t.Format("2006-01-02 15:04:05")
	formatted = strings.Replace(formatted, "-", "", -1)
	formatted = strings.Replace(formatted, ":", "", -1)
	formatted = strings.Replace(formatted, " ", ":", -1)
	formatted = fmt.Sprint(formatted, "_", fileName)
	return formatted
}

func (s *s3Handler) GetUploadObject(file *multipart.FileHeader, folder string) (string, []byte, error) {
	openedFile, err := file.Open()
	if err != nil {
		return "", nil, fmt.Errorf("error on file open %s", err.Error())
	}

	fileData, err := ioutil.ReadAll(openedFile)
	if err != nil {
		return "", nil, fmt.Errorf("error on file read %s", err.Error())
	}

	s3InputName := s.GetS3InputName(file.Filename)
	bucketKey := s.GetS3Key(folder, s3InputName)
	return bucketKey, fileData, nil
}
