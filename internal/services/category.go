package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type categoryService struct {
	Repo domain.CategoryRepository
}

type CategoryService interface {
	Get(ctx context.Context) ([]*CategoryGetModel, error)
	GetById(ctx context.Context, id uuid.UUID) (*CategoryGetModel, error)
	Add(ctx context.Context, model *CategoryAddModel) error
	Update(ctx context.Context, model *CategoryUpdateModel) error
	Delete(ctx context.Context, id uuid.UUID) error
}

func NewCategoryService(repo domain.CategoryRepository) CategoryService {
	return &categoryService{
		Repo: repo,
	}
}

func (cs *categoryService) Get(ctx context.Context) ([]*CategoryGetModel, error) {
	data, err := cs.Repo.Get(ctx)
	if err != nil {
		return nil, err
	}

	var categories []*CategoryGetModel
	for _, d := range data {
		c := &CategoryGetModel{
			Id:          d.ID,
			Name:        d.Name,
			Description: d.Description,
		}
		categories = append(categories, c)
	}
	return categories, nil
}

func (cs *categoryService) GetById(ctx context.Context, id uuid.UUID) (*CategoryGetModel, error) {
	data, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return nil, err
	}

	category := &CategoryGetModel{
		Id:          data.ID,
		Name:        data.Name,
		Description: data.Description,
	}
	return category, nil
}

func (cs *categoryService) Add(ctx context.Context, model *CategoryAddModel) error {
	category := &domain.Category{
		Name:        model.Name,
		Description: model.Description,
	}
	return cs.Repo.Save(ctx, category)
}

func (cs *categoryService) Update(ctx context.Context, model *CategoryUpdateModel) error {
	category, err := cs.Repo.GetById(ctx, model.Id)
	if err != nil {
		return fmt.Errorf("error on get by id repo call: %s", err.Error())
	}

	category.Name = model.Description
	category.Description = model.Description
	return cs.Repo.Save(ctx, category)
}

func (cs *categoryService) Delete(ctx context.Context, id uuid.UUID) error {
	category, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return err
	}
	return cs.Repo.Delete(ctx, category)
}

type CategoryGetModel struct {
	Id          uuid.UUID
	Name        string
	Description string
}

type CategoryAddModel struct {
	Name        string
	Description string
}

type CategoryUpdateModel struct {
	Id          uuid.UUID
	Name        string
	Description string
}
