package services

import (
	"context"
	"fmt"
	"mime/multipart"

	"github.com/aydinmuzaffer/sofram-notification/internal/cloud"
	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	soframerror "github.com/aydinmuzaffer/sofram-notification/internal/error"
	"github.com/aydinmuzaffer/sofram-notification/internal/helper/filehelper"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
)

var emailTemplateBucketFolder = "email_layout"

type emailTemplateService struct {
	Repo         domain.EmailTemplateRepository
	S3           cloud.S3Bucket
	TemplateRepo domain.TemplateRepository
}

type EmailTemplateService interface {
	GetByNotificationId(ctx context.Context, notificationTypeId uuid.UUID) (*EmailTemplateQueryModel, error)
	Add(ctx context.Context, model *EmailTemplateAddModel) (*EmailTemplateGetModel, error)
	Update(ctx context.Context, model *EmailTemplateUpdateModel) (*EmailTemplateGetModel, error)
	Delete(ctx context.Context, id uuid.UUID) error
}

func NewEmailTemplateService(repo domain.EmailTemplateRepository, s3 cloud.S3Bucket, templateRepo domain.TemplateRepository) EmailTemplateService {
	return &emailTemplateService{
		Repo:         repo,
		S3:           s3,
		TemplateRepo: templateRepo,
	}
}

func (ets *emailTemplateService) GetByNotificationId(ctx context.Context, notificationTypeId uuid.UUID) (*EmailTemplateQueryModel, error) {
	t, err := ets.Repo.GetByNotificationId(ctx, notificationTypeId)
	if err != nil {
		return nil, err
	}
	return converEmailTemplateQueryModelToServiceQueryModel(t), nil
}
func (ets *emailTemplateService) Add(ctx context.Context, model *EmailTemplateAddModel) (*EmailTemplateGetModel, error) {
	notificationTypeId, _ := uuid.FromString(model.NotificatioTypeId)
	template, err := ets.TemplateRepo.GetByNotificationTypeId(ctx, notificationTypeId)
	if err != nil && errors.Is(err, soframerror.ErrRecordNotFound) {
		newTemplate := &domain.Template{}
		newTemplate.Name = model.Name
		newTemplate.NotificationTypeId = notificationTypeId
		if err := ets.TemplateRepo.Save(ctx, newTemplate); err != nil {
			return nil, err
		}
		template = newTemplate
	}
	bucketKey, fileData, err := ets.S3.GetUploadObject(model.File, emailTemplateBucketFolder)
	if err != nil {
		return nil, err
	}
	contentType, err := filehelper.GetFileContentType(model.File)
	if err != nil {
		return nil, err
	}
	uploadOutput, err := ets.S3.Upload(ctx, bucketKey, fileData, contentType)
	if err != nil {
		return nil, fmt.Errorf("error on email layout upload: %s", err.Error())
	}

	//TODO: find a way to getting uuid values without not converting them manually
	emailLayoutId, _ := uuid.FromString(model.EmailLayoutId)
	provierId, _ := uuid.FromString(model.ProviderId)
	languageId, _ := uuid.FromString(model.LanguageId)
	emailTemplate := &domain.EmailTemplate{
		TemplateId:    template.ID,
		EmailLayoutId: emailLayoutId,
		ProviderId:    provierId,
		BucketKey:     bucketKey,
		Location:      uploadOutput.Location,
		LanguageId:    languageId,
		Subject:       model.Subject,
	}

	if err := ets.Repo.Save(ctx, emailTemplate); err != nil {
		return nil, err
	}

	return &EmailTemplateGetModel{
		Id:            emailTemplate.ID,
		EmailLayoutId: emailTemplate.EmailLayoutId,
		ProviderId:    emailTemplate.ProviderId,
		BucketKey:     emailTemplate.BucketKey,
		Location:      emailTemplate.Location,
		Subject:       emailTemplate.Subject,
		LanguageId:    emailTemplate.LanguageId,
	}, nil
}
func (ets *emailTemplateService) Update(ctx context.Context, model *EmailTemplateUpdateModel) (*EmailTemplateGetModel, error) {
	notificationTypeId, _ := uuid.FromString(model.NotificatioTypeId)
	template, err := ets.TemplateRepo.GetByNotificationTypeId(ctx, notificationTypeId)
	if err != nil {
		return nil, fmt.Errorf("error on get by id repo call: %s", err.Error())
	}
	id, _ := uuid.FromString(model.Id)
	emailTemplate, err := ets.Repo.GetById(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("error on get by id repo call: %s", err.Error())
	}
	oldBucketKey := emailTemplate.BucketKey
	bucketKey, fileData, err := ets.S3.GetUploadObject(model.File, emailTemplateBucketFolder)
	if err != nil {
		return nil, err
	}
	contentType, err := filehelper.GetFileContentType(model.File)
	if err != nil {
		return nil, err
	}
	uploadOutput, err := ets.S3.Upload(ctx, bucketKey, fileData, contentType)
	if err != nil {
		return nil, fmt.Errorf("error on email layout upload: %s", err.Error())
	}

	emailLayoutId, _ := uuid.FromString(model.EmailLayoutId)
	provierId, _ := uuid.FromString(model.ProviderId)
	languageId, _ := uuid.FromString(model.LanguageId)
	emailTemplate.EmailLayoutId = emailLayoutId
	emailTemplate.ProviderId = provierId
	emailTemplate.BucketKey = bucketKey
	emailTemplate.Location = uploadOutput.Location
	emailTemplate.Subject = model.Subject
	emailTemplate.LanguageId = languageId
	template.Name = model.Name
	if err := ets.TemplateRepo.Save(ctx, template); err != nil {
		return nil, err
	}
	if err := ets.Repo.Save(ctx, emailTemplate); err != nil {
		return nil, err
	}

	err = ets.S3.Delete(ctx, oldBucketKey)
	if err != nil {
		return nil, fmt.Errorf("error on email layout delete: %s", err.Error())
	}

	return &EmailTemplateGetModel{
		Id:            emailTemplate.ID,
		EmailLayoutId: emailTemplate.EmailLayoutId,
		ProviderId:    emailTemplate.ProviderId,
		BucketKey:     emailTemplate.BucketKey,
		Location:      emailTemplate.Location,
		LanguageId:    emailTemplate.LanguageId,
		Subject:       emailTemplate.Subject,
	}, nil
}
func (cs *emailTemplateService) Delete(ctx context.Context, id uuid.UUID) error {
	emailTemplate, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return err
	}
	if err := cs.Repo.Delete(ctx, emailTemplate); err != nil {
		return err
	}

	err = cs.S3.Delete(ctx, emailTemplate.BucketKey)
	if err != nil {
		return fmt.Errorf("error on email layout delete: %s", err.Error())
	}
	return nil
}

func converEmailTemplateQueryModelToServiceQueryModel(m *domain.EmailTemplateQueryModel) *EmailTemplateQueryModel {
	return &EmailTemplateQueryModel{
		ID:                     m.ID,
		TemplateId:             m.TemplateId,
		TemplateName:           m.TemplateName.String,
		NotificationTypeId:     m.NotificationTypeId,
		NotificationType:       m.NotificationType.String,
		EmailLayoutId:          m.EmailLayoutId,
		EmailLayoutDescription: m.EmailLayoutDescription.String,
		EmailLayoutLocation:    m.EmailLayoutLocation.String,
		ProviderId:             m.ProviderId,
		ProviderName:           m.ProviderName.String,
		BucketKey:              m.BucketKey.String,
		Location:               m.Location.String,
		LanguageId:             m.LanguageId,
		LanguageCode:           m.LanguageCode.String,
		Subject:                m.Subject.String,
	}
}

type EmailTemplateGetModel struct {
	Id            uuid.UUID `json:"id"`
	EmailLayoutId uuid.UUID `json:"email_layout_id"`
	ProviderId    uuid.UUID `json:"provider_id"`
	LanguageId    uuid.UUID `json:"language_id"`
	Subject       string    `json:"subject"`
	BucketKey     string    `json:"bucket_key"`
	Location      string    `json:"location"`
}

type EmailTemplateQueryModel struct {
	ID                     uuid.UUID `json:"id"`
	TemplateId             uuid.UUID `json:"template_id"`
	TemplateName           string    `json:"template_name"`
	NotificationTypeId     uuid.UUID `json:"notification_type_id"`
	NotificationType       string    `json:"notification_type"`
	EmailLayoutId          uuid.UUID `json:"email_layout_id"`
	EmailLayoutDescription string    `json:"email_layout_description"`
	EmailLayoutLocation    string    `json:"email_layout_location"`
	ProviderId             uuid.UUID `json:"provider_id"`
	ProviderName           string    `json:"provider_name"`
	BucketKey              string    `json:"bucket_key"`
	Location               string    `json:"location"`
	LanguageId             uuid.UUID `json:"language_id"`
	LanguageCode           string    `json:"language_code"`
	Subject                string    `json:"subject"`
}

type EmailTemplateAddModel struct {
	Name              string                `form:"name" json:"name"`
	NotificatioTypeId string                `form:"notification_type_id" json:"notification_type_id"`
	EmailLayoutId     string                `form:"email_layout_id" json:"email_layout_id"`
	ProviderId        string                `form:"provider_id" json:"provider_id"`
	LanguageId        string                `form:"language_id" json:"language_id"`
	Subject           string                `form:"subject" json:"subject"`
	File              *multipart.FileHeader `form:"file" binding:"required" swaggerignore:"true"`
}

type EmailTemplateUpdateModel struct {
	Id                string                `form:"id" json:"id"`
	Name              string                `form:"name" json:"name"`
	NotificatioTypeId string                `form:"notification_type_id" json:"notification_type_id"`
	EmailLayoutId     string                `form:"email_layout_id" json:"email_layout_id"`
	ProviderId        string                `form:"provider_id" json:"provider_id"`
	LanguageId        string                `form:"language_id" json:"language_id"`
	Subject           string                `form:"subject" json:"subject"`
	File              *multipart.FileHeader `form:"file" binding:"required" swaggerignore:"true"`
}
