package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type languageService struct {
	Repo domain.LanguageRepository
}

type LanguageService interface {
	Get(ctx context.Context, code string) ([]*LanguageGetModel, error)
	GetById(ctx context.Context, id uuid.UUID) (*LanguageGetModel, error)
	Add(ctx context.Context, model *LanguageAddModel) (*LanguageGetModel, error)
	Update(ctx context.Context, model *LanguageUpdateModel) (*LanguageGetModel, error)
	Delete(ctx context.Context, code string) error
}

func NewLanguageService(repo domain.LanguageRepository) LanguageService {
	return &languageService{
		Repo: repo,
	}
}

func (cs *languageService) Get(ctx context.Context, code string) ([]*LanguageGetModel, error) {
	data, err := cs.Repo.Get(ctx, code)
	if err != nil {
		return nil, err
	}

	var languages []*LanguageGetModel
	for _, d := range data {
		c := &LanguageGetModel{
			Id:   d.ID,
			Name: d.Name,
			Code: d.Code,
		}
		languages = append(languages, c)
	}
	return languages, nil
}
func (cs *languageService) GetById(ctx context.Context, id uuid.UUID) (*LanguageGetModel, error) {
	data, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return nil, err
	}

	language := &LanguageGetModel{
		Id:   data.ID,
		Name: data.Name,
		Code: data.Code,
	}

	return language, nil
}
func (cs *languageService) Add(ctx context.Context, model *LanguageAddModel) (*LanguageGetModel, error) {
	language := &domain.Language{
		Name: model.Name,
		Code: model.Code,
	}
	if err := cs.Repo.Save(ctx, language); err != nil {
		return nil, err
	}
	return &LanguageGetModel{
		Id:   language.ID,
		Name: language.Name,
		Code: language.Code,
	}, nil
}
func (cs *languageService) Update(ctx context.Context, model *LanguageUpdateModel) (*LanguageGetModel, error) {
	language, err := cs.Repo.GetByCode(ctx, model.Code)
	if err != nil {
		return nil, fmt.Errorf("error on get by id repo call: %s", err.Error())
	}

	language.Name = model.Name
	language.Code = model.Code
	if err := cs.Repo.Save(ctx, language); err != nil {
		return nil, err
	}

	return &LanguageGetModel{
		Id:   language.ID,
		Name: language.Name,
		Code: language.Code,
	}, nil
}
func (cs *languageService) Delete(ctx context.Context, code string) error {
	language, err := cs.Repo.GetByCode(ctx, code)
	if err != nil {
		return err
	}
	return cs.Repo.Delete(ctx, language)
}

type LanguageGetModel struct {
	Id   uuid.UUID
	Name string
	Code string
}

type LanguageAddModel struct {
	Name string
	Code string
}

type LanguageUpdateModel struct {
	Id   uuid.UUID
	Name string
	Code string
}
