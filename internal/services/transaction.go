package services

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type transactionService struct {
	Repo domain.TransactionRepository
}

type TransactionService interface {
	Add(ctx context.Context, model TransactionAddModel) error
}

func NewTransactionService(repo domain.TransactionRepository) TransactionService {
	return &transactionService{
		Repo: repo,
	}
}

func (r *transactionService) Add(ctx context.Context, model TransactionAddModel) error {
	transaction := &domain.Transaction{
		NotificationId:     model.NotificationId,
		ProviderReturnCode: model.ProviderReturnCode,
		ProviderMessage:    model.ProviderMessage,
		ProviderMessageId:  model.ProviderMessageId,
	}

	return r.Repo.Save(ctx, transaction)
}

type TransactionAddModel struct {
	NotificationId     uuid.UUID
	ProviderReturnCode string
	ProviderMessage    string
	ProviderMessageId  string
}
