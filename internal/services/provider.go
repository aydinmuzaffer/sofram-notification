package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type providerService struct {
	repo       domain.ProviderRepository
	chanelRepo domain.ChanelRepository
}

type ProviderService interface {
	Get(ctx context.Context) ([]ProviderGetModel, error)
}

func NewProviderService(providerRepostiory domain.ProviderRepository, chanelRepository domain.ChanelRepository) ProviderService {
	return &providerService{
		repo:       providerRepostiory,
		chanelRepo: chanelRepository,
	}
}

func (rs *providerService) Get(ctx context.Context) ([]ProviderGetModel, error) {

	providers, err := rs.repo.Get(ctx)
	if err != nil {
		return []ProviderGetModel{}, fmt.Errorf("provider getall: %w", err)
	}

	chanels, err := rs.chanelRepo.Get(ctx)
	if err != nil {
		return []ProviderGetModel{}, fmt.Errorf("provider getall: %w", err)
	}

	providersToReturn := []ProviderGetModel{}

	for _, p := range providers {
		provider := ProviderGetModel{
			Id:          p.ID,
			Name:        p.Name,
			Description: p.Description,
		}
		for _, chanel := range chanels {
			if chanel.ID == p.ChanelId {
				provider.Chanel = chanel.Name
				break
			}
		}
		providersToReturn = append(providersToReturn, provider)
	}

	return providersToReturn, nil
}

type ProviderGetModel struct {
	Id          uuid.UUID
	Name        string
	Description string
	Chanel      string
}
