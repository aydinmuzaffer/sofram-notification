package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type notificationTypeService struct {
	Repo domain.NotificationTypeRepository
}

type NotificationTypeService interface {
	Get(ctx context.Context, name string) ([]*NotificationTypeGetModel, error)
	GetById(ctx context.Context, id uuid.UUID) (*NotificationTypeGetModel, error)
	Add(ctx context.Context, model *NotificationTypeAddModel) (*NotificationTypeGetModel, error)
	Update(ctx context.Context, model *NotificationTypeUpdateModel) (*NotificationTypeGetModel, error)
	Delete(ctx context.Context, id uuid.UUID) error
}

func NewNotificationTypeService(repo domain.NotificationTypeRepository) NotificationTypeService {
	return &notificationTypeService{
		Repo: repo,
	}
}

func (cs *notificationTypeService) Get(ctx context.Context, name string) ([]*NotificationTypeGetModel, error) {
	data, err := cs.Repo.Get(ctx, name)
	if err != nil {
		return nil, err
	}

	var notificationTypes []*NotificationTypeGetModel
	for _, d := range data {
		c := convertToNotificationTypeGetModel(d)
		notificationTypes = append(notificationTypes, c)
	}
	return notificationTypes, nil
}

func (cs *notificationTypeService) GetById(ctx context.Context, id uuid.UUID) (*NotificationTypeGetModel, error) {
	data, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("error on get by id repo call: %s", err.Error())
	}

	return convertToNotificationTypeGetModel(data), nil
}

func (cs *notificationTypeService) Add(ctx context.Context, model *NotificationTypeAddModel) (*NotificationTypeGetModel, error) {

	notificationType := &domain.NotificationType{
		Name:        model.Name,
		Description: model.Description,
		CategoryId:  model.CategoryId,
	}
	if err := cs.Repo.Save(ctx, notificationType); err != nil {
		return nil, err
	}

	return convertToNotificationTypeGetModel(notificationType), nil
}

func convertToNotificationTypeGetModel(notificationType *domain.NotificationType) *NotificationTypeGetModel {
	return &NotificationTypeGetModel{
		Id:          notificationType.ID,
		Name:        notificationType.Name,
		Description: notificationType.Description,
		CategoryId:  notificationType.CategoryId,
	}
}
func (cs *notificationTypeService) Update(ctx context.Context, model *NotificationTypeUpdateModel) (*NotificationTypeGetModel, error) {
	notificationType, err := cs.Repo.GetById(ctx, model.Id)
	if err != nil {
		return nil, fmt.Errorf("error on get by id repo call: %s", err.Error())
	}

	notificationType.Name = model.Name
	notificationType.Description = model.Description
	notificationType.CategoryId = model.CategoryId
	if err := cs.Repo.Save(ctx, notificationType); err != nil {
		return nil, err
	}

	return convertToNotificationTypeGetModel(notificationType), nil
}

func (cs *notificationTypeService) Delete(ctx context.Context, id uuid.UUID) error {
	notificationType, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return err
	}
	return cs.Repo.Delete(ctx, notificationType)
}

type NotificationTypeGetModel struct {
	Id          uuid.UUID
	Name        string
	Description string
	CategoryId  uuid.UUID
}

type NotificationTypeAddModel struct {
	Name        string
	Description string
	CategoryId  uuid.UUID
}

type NotificationTypeUpdateModel struct {
	Id          uuid.UUID
	Name        string
	Description string
	CategoryId  uuid.UUID
}
