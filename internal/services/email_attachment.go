package services

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type emailAttachmentService struct {
	Repo domain.EmailAttachmentRepository
}

type EmailAttachmentService interface {
	Add(ctx context.Context, model *EmailAttachmentAddModel) error
}

func NewEmailAttachmentService(repo domain.EmailAttachmentRepository) EmailAttachmentService {
	return &emailAttachmentService{
		Repo: repo,
	}
}

func (cs *emailAttachmentService) Add(ctx context.Context, model *EmailAttachmentAddModel) error {

	emailAttachment := &domain.EmailAttachment{
		Name:           model.Name,
		NotificationId: model.NotificationId,
		BucketKey:      model.BucketKey,
	}
	return cs.Repo.Save(ctx, emailAttachment)
}

type EmailAttachmentAddModel struct {
	NotificationId uuid.UUID
	Name           string
	BucketKey      string
}
