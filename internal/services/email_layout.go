package services

import (
	"context"
	"fmt"
	"mime/multipart"

	"github.com/aydinmuzaffer/sofram-notification/internal/cloud"
	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	"github.com/aydinmuzaffer/sofram-notification/internal/helper/filehelper"
	uuid "github.com/satori/go.uuid"
)

var emailLayoutBucketFolder = "email_layout"

type emailLayoutService struct {
	Repo domain.EmailLayoutRepository
	S3   cloud.S3Bucket
}

type EmailLayoutService interface {
	Get(ctx context.Context, name string) ([]*EmailLayoutGetModel, error)
	GetById(ctx context.Context, id uuid.UUID) (*EmailLayoutGetModel, error)
	Add(ctx context.Context, model *EmailLayoutAddModel) (*EmailLayoutGetModel, error)
	Update(ctx context.Context, model *EmailLayoutUpdateModel) (*EmailLayoutGetModel, error)
	Delete(ctx context.Context, id uuid.UUID) error
}

func NewEmailLayoutService(repo domain.EmailLayoutRepository, s3 cloud.S3Bucket) EmailLayoutService {
	return &emailLayoutService{
		Repo: repo,
		S3:   s3,
	}
}

func (cs *emailLayoutService) Get(ctx context.Context, name string) ([]*EmailLayoutGetModel, error) {
	data, err := cs.Repo.Get(ctx, name)
	if err != nil {
		return nil, err
	}

	var emailLayouts []*EmailLayoutGetModel
	for _, d := range data {
		c := convertToEmailLayoutGetModel(d)
		emailLayouts = append(emailLayouts, c)
	}
	return emailLayouts, nil
}

func (cs *emailLayoutService) GetById(ctx context.Context, id uuid.UUID) (*EmailLayoutGetModel, error) {
	data, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return nil, err
	}
	return convertToEmailLayoutGetModel(data), nil
}

func (cs *emailLayoutService) Add(ctx context.Context, model *EmailLayoutAddModel) (*EmailLayoutGetModel, error) {
	bucketKey, fileData, err := cs.S3.GetUploadObject(model.File, emailLayoutBucketFolder)
	if err != nil {
		return nil, err
	}
	contentType, err := filehelper.GetFileContentType(model.File)
	if err != nil {
		return nil, err
	}
	uploadOutput, err := cs.S3.Upload(ctx, bucketKey, fileData, contentType)
	if err != nil {
		return nil, fmt.Errorf("error on email layout upload: %s", err.Error())
	}

	emailLayout := &domain.EmailLayout{
		Name:        model.Name,
		Description: model.Description,
		BucketKey:   bucketKey,
		Location:    uploadOutput.Location,
	}
	if err := cs.Repo.Save(ctx, emailLayout); err != nil {
		return nil, err
	}
	return convertToEmailLayoutGetModel(emailLayout), nil
}

func convertToEmailLayoutGetModel(emailLayout *domain.EmailLayout) *EmailLayoutGetModel {
	return &EmailLayoutGetModel{
		Id:          emailLayout.ID,
		Name:        emailLayout.Name,
		Description: emailLayout.Description,
		BucketKey:   emailLayout.BucketKey,
		Location:    emailLayout.Location,
	}
}

func (cs *emailLayoutService) Update(ctx context.Context, model *EmailLayoutUpdateModel) (*EmailLayoutGetModel, error) {
	id, err := uuid.FromString(model.Id)
	if err != nil {
		return nil, err
	}
	emailLayout, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("error on get by id repo call: %s", err.Error())
	}
	oldBucketKey := emailLayout.BucketKey
	bucketKey, fileData, err := cs.S3.GetUploadObject(model.File, emailLayoutBucketFolder)
	if err != nil {
		return nil, err
	}
	contentType, err := filehelper.GetFileContentType(model.File)
	if err != nil {
		return nil, err
	}
	uploadOutput, err := cs.S3.Upload(ctx, bucketKey, fileData, contentType)
	if err != nil {
		return nil, fmt.Errorf("error on email layout upload: %s", err.Error())
	}

	emailLayout.Name = model.Name
	emailLayout.Description = model.Description
	emailLayout.BucketKey = bucketKey
	emailLayout.Location = uploadOutput.Location
	if err := cs.Repo.Save(ctx, emailLayout); err != nil {
		return nil, err
	}

	err = cs.S3.Delete(ctx, oldBucketKey)
	if err != nil {
		return nil, fmt.Errorf("error on email layout delete: %s", err.Error())
	}
	return convertToEmailLayoutGetModel(emailLayout), nil
}

func (cs *emailLayoutService) Delete(ctx context.Context, id uuid.UUID) error {
	emailLayout, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return err
	}

	if err := cs.Repo.Delete(ctx, emailLayout); err != nil {
		return err
	}

	err = cs.S3.Delete(ctx, emailLayout.BucketKey)
	if err != nil {
		return fmt.Errorf("error on email layout delete: %s", err.Error())
	}
	return nil
}

type EmailLayoutGetModel struct {
	Id          uuid.UUID `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	BucketKey   string    `json:"bucket_key"`
	Location    string    `json:"location"`
}

type EmailLayoutAddModel struct {
	Name        string                `form:"name" json:"name"`
	Description string                `form:"description" json:"description"`
	File        *multipart.FileHeader `form:"file" binding:"required" swaggerignore:"true"`
}

type EmailLayoutUpdateModel struct {
	Id          string                `form:"id" json:"id"`
	Name        string                `form:"name" json:"name"`
	Description string                `form:"description" json:"description"`
	File        *multipart.FileHeader `form:"file" binding:"required" swaggerignore:"true"`
}
