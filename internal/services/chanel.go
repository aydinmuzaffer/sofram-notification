package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type chanelService struct {
	repo domain.ChanelRepository
}

type ChanelService interface {
	Get(ctx context.Context) ([]ChanelGetModel, error)
}

func NewChanelService(chanelRepostiory domain.ChanelRepository) ChanelService {
	return &chanelService{
		repo: chanelRepostiory,
	}
}

func (rs *chanelService) Get(ctx context.Context) ([]ChanelGetModel, error) {

	chanels, err := rs.repo.Get(ctx)
	if err != nil {
		return []ChanelGetModel{}, fmt.Errorf("chanel getall: %w", err)
	}

	chanelsToReturn := []ChanelGetModel{}

	for _, c := range chanels {
		chanel := ChanelGetModel{
			Id:          c.ID,
			Name:        c.Name,
			Description: c.Description,
		}
		chanelsToReturn = append(chanelsToReturn, chanel)
	}

	return chanelsToReturn, nil
}

type ChanelGetModel struct {
	Id          uuid.UUID
	Name        string
	Description string
}
