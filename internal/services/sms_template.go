package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type smsTemplateService struct {
	Repo domain.SmsTemplateRepository
}

type SmsTemplateService interface {
	Get(ctx context.Context) ([]*SmsTemplateGetModel, error)
	GetById(ctx context.Context, id uuid.UUID) (*SmsTemplateGetModel, error)
	GetByNotificationTypeId(ctx context.Context, notificationTypeId uuid.UUID) (*SmsTemplateQueryModel, error)
	Add(ctx context.Context, model *SmsTemplateAddModel) (*SmsTemplateGetModel, error)
	Update(ctx context.Context, model *SmsTemplateUpdateModel) error
	Delete(ctx context.Context, id uuid.UUID) error
}

func NewSmsTemplateService(repo domain.SmsTemplateRepository) SmsTemplateService {
	return &smsTemplateService{
		Repo: repo,
	}
}

func (cs *smsTemplateService) Get(ctx context.Context) ([]*SmsTemplateGetModel, error) {
	data, err := cs.Repo.Get(ctx)
	if err != nil {
		return nil, err
	}

	var smsTemplates []*SmsTemplateGetModel
	for _, d := range data {
		c := &SmsTemplateGetModel{
			Id:         d.ID,
			TemplateId: d.TemplateId,
			Content:    d.Content,
			ProviderId: d.ProviderId,
		}
		smsTemplates = append(smsTemplates, c)
	}
	return smsTemplates, nil
}
func (cs *smsTemplateService) GetById(ctx context.Context, id uuid.UUID) (*SmsTemplateGetModel, error) {
	t, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return nil, err
	}

	template := &SmsTemplateGetModel{
		Id:         t.ID,
		TemplateId: t.TemplateId,
		Content:    t.Content,
		ProviderId: t.ProviderId,
	}
	return template, nil

}

func (cs *smsTemplateService) GetByNotificationTypeId(ctx context.Context, notificationTypeId uuid.UUID) (*SmsTemplateQueryModel, error) {
	t, err := cs.Repo.GetByNotificationTypeId(ctx, notificationTypeId)
	if err != nil {
		return nil, err
	}

	return convertSmsTemplateQueryModelToServiceQueryModel(t), nil
}
func convertSmsTemplateQueryModelToServiceQueryModel(s *domain.SmsTemplateQueryModel) *SmsTemplateQueryModel {
	return &SmsTemplateQueryModel{
		ID:                 s.ID,
		TemplateId:         s.TemplateId,
		TemplateName:       s.TemplateName.String,
		NotificationTypeId: s.NotificationTypeId,
		ProviderId:         s.ProviderId,
		ProviderName:       s.ProviderName.String,
		LanguageId:         s.LanguageId,
		LanguageCode:       s.LanguageCode.String,
		Content:            s.Content.String,
	}
}
func (cs *smsTemplateService) Add(ctx context.Context, model *SmsTemplateAddModel) (*SmsTemplateGetModel, error) {
	smsTemplate := &domain.SmsTemplate{
		TemplateId: model.TemplateId,
		Content:    model.Content,
		ProviderId: model.ProviderId,
		LanguageId: model.LanguageId,
	}

	if err := cs.Repo.Save(ctx, smsTemplate); err != nil {
		return nil, err
	}

	return &SmsTemplateGetModel{
		Id:         smsTemplate.ID,
		TemplateId: model.TemplateId,
		ProviderId: model.ProviderId,
		Content:    model.Content,
		LanguageId: model.LanguageId,
	}, nil
}
func (cs *smsTemplateService) Update(ctx context.Context, model *SmsTemplateUpdateModel) error {
	smsTemplate, err := cs.Repo.GetById(ctx, model.Id)
	if err != nil {
		return fmt.Errorf("error on get by id repo call: %s", err.Error())
	}

	smsTemplate.TemplateId = model.TemplateId
	smsTemplate.Content = string(model.Content)
	smsTemplate.ProviderId = model.ProviderId
	return cs.Repo.Save(ctx, smsTemplate)
}
func (cs *smsTemplateService) Delete(ctx context.Context, id uuid.UUID) error {
	smsTemplate, err := cs.Repo.GetById(ctx, id)
	if err != nil {
		return err
	}
	return cs.Repo.Delete(ctx, smsTemplate)
}

type SmsTemplateGetModel struct {
	Id         uuid.UUID
	TemplateId uuid.UUID
	ProviderId uuid.UUID
	LanguageId uuid.UUID
	Content    string
}

type SmsTemplateAddModel struct {
	TemplateId uuid.UUID
	ProviderId uuid.UUID
	LanguageId uuid.UUID
	Content    string
}

type SmsTemplateUpdateModel struct {
	Id         uuid.UUID
	TemplateId uuid.UUID
	ProviderId uuid.UUID
	LanguageId uuid.UUID
	Content    string
}

type SmsTemplateQueryModel struct {
	ID                 uuid.UUID
	TemplateId         uuid.UUID
	TemplateName       string
	NotificationTypeId uuid.UUID
	NotificationType   string
	ProviderId         uuid.UUID
	ProviderName       string
	LanguageId         uuid.UUID
	LanguageCode       string
	Content            string
}
