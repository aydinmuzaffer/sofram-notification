package services

import (
	"context"
	"fmt"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type recipientTypeService struct {
	repo domain.RecipientTypeRepository
}

type RecipientTypeService interface {
	Get(ctx context.Context) ([]RecipientTypeGetModel, error)
}

func NewRecipientTypeService(recipientTypeRepostiory domain.RecipientTypeRepository) RecipientTypeService {
	return &recipientTypeService{
		repo: recipientTypeRepostiory,
	}
}

func (rs *recipientTypeService) Get(ctx context.Context) ([]RecipientTypeGetModel, error) {

	recipientTypes, err := rs.repo.Get(ctx)
	if err != nil {
		return []RecipientTypeGetModel{}, fmt.Errorf("recipientType getall: %w", err)
	}

	recipientTypesToReturn := []RecipientTypeGetModel{}

	for _, c := range recipientTypes {
		recipientType := RecipientTypeGetModel{
			Id:          c.ID,
			Name:        c.Name,
			Description: c.Description,
		}
		recipientTypesToReturn = append(recipientTypesToReturn, recipientType)
	}

	return recipientTypesToReturn, nil
}

type RecipientTypeGetModel struct {
	Id          uuid.UUID
	Name        string
	Description string
}
