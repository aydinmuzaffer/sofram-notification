package services

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type transactionRecipientService struct {
	Repo domain.TransactionRecipientRepository
}

type TransactionRecipientService interface {
	Add(ctx context.Context, model TransactionRecipientAddModel) error
}

func NewTransactionRecipientService(repo domain.TransactionRecipientRepository) TransactionRecipientService {
	return &transactionRecipientService{
		Repo: repo,
	}
}

func (r *transactionRecipientService) Add(ctx context.Context, model TransactionRecipientAddModel) error {
	transactionRecipient := &domain.TransactionRecipient{
		TransactionId:   model.TransactionId,
		Recipient:       model.Recipient,
		RecipientTypeId: model.RecipientTypeId,
	}

	return r.Repo.Save(ctx, transactionRecipient)
}

type TransactionRecipientAddModel struct {
	TransactionId   uuid.UUID
	Recipient       string
	RecipientTypeId uuid.UUID
}
