package services

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type notificationService struct {
	Repo domain.NotificationRepository
}

type NotificationService interface {
	Add(ctx context.Context, model NotificationAddModel) error
}

func NewNotificationService(repo domain.NotificationRepository) NotificationService {
	return &notificationService{
		Repo: repo,
	}
}

func (r *notificationService) Add(ctx context.Context, model NotificationAddModel) error {
	notification := &domain.Notification{
		NotificationTypeId: model.NotificationTypeId,
		Name:               model.Name,
		Subject:            model.Subject,
		LanguageId:         model.LanguageId,
		ProviderId:         model.ProviderId,
		ChanelId:           model.ChanelId,
		BucketKey:          model.BucketKey,
	}

	return r.Repo.Save(ctx, notification)
}

type NotificationAddModel struct {
	NotificationTypeId uuid.UUID
	Name               string
	Subject            string
	LanguageId         uuid.UUID
	ProviderId         uuid.UUID
	ChanelId           uuid.UUID
	BucketKey          string
}
