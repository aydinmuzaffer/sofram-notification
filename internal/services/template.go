package services

import (
	"context"

	"github.com/aydinmuzaffer/sofram-notification/internal/domain"
	uuid "github.com/satori/go.uuid"
)

type templateService struct {
	Repo domain.TemplateRepository
}

type TemplateService interface {
	GetById(ctx context.Context, id uuid.UUID) (*TemplateQueryModel, error)
	Delete(ctx context.Context, id uuid.UUID) error
}

func NewTemplateService(repo domain.TemplateRepository) TemplateService {
	return &templateService{
		Repo: repo,
	}
}

func (cs *templateService) GetById(ctx context.Context, notificationTypeId uuid.UUID) (*TemplateQueryModel, error) {
	t, err := cs.Repo.GetById(ctx, notificationTypeId)
	if err != nil {
		return nil, err
	}
	return conveTemplateQueryModelToServiceQueryModel(t), nil
}

func (cs *templateService) Delete(ctx context.Context, notificationTypeId uuid.UUID) error {
	template, err := cs.Repo.GetByNotificationTypeId(ctx, notificationTypeId)
	if err != nil {
		return err
	}
	return cs.Repo.Delete(ctx, template)
}

func conveTemplateQueryModelToServiceQueryModel(m *domain.TemplateQueryModel) *TemplateQueryModel {
	template := &TemplateQueryModel{}
	template.Sms = make([]*SmsTemplateQueryModel, 0)
	template.Email = make([]*EmailTemplateQueryModel, 0)

	template.Name = m.Name
	template.NotificationTypeId = m.NotificationTypeId
	template.NotificationType = m.NotificationType

	for _, v := range m.Sms {
		sms := convertSmsTemplateQueryModelToServiceQueryModel(v)
		sms.TemplateName = m.Name
		sms.TemplateId = m.Id
		sms.NotificationTypeId = m.NotificationTypeId
		sms.NotificationType = m.NotificationType
		template.Sms = append(template.Sms, sms)
	}
	for _, v := range m.Email {
		email := converEmailTemplateQueryModelToServiceQueryModel(v)
		email.TemplateName = m.Name
		email.TemplateId = m.Id
		email.NotificationTypeId = m.NotificationTypeId
		email.NotificationType = m.NotificationType
		template.Email = append(template.Email, email)
	}

	return template
}

type TemplateQueryModel struct {
	Id                 uuid.UUID                  `json:"id"`
	NotificationTypeId uuid.UUID                  `json:"notification_type_id"`
	NotificationType   string                     `json:"notification_type"`
	Name               string                     `json:"name"`
	Sms                []*SmsTemplateQueryModel   `json:"sms"`
	Email              []*EmailTemplateQueryModel `json:"email"`
}
