package domain

import (
	"context"
)

type RecipientType struct {
	Enumeration
}

func (RecipientType) TableName() string {
	return "recipient_type"
}

type RecipientTypeRepository interface {
	Get(ctx context.Context) ([]*RecipientType, error)
}

func (p Enumeration) ConvertToRecipientType() *RecipientType {
	return &RecipientType{p}
}

var RecipientTypeEnum = struct {
	From *RecipientType
	To   *RecipientType
	Cc   *RecipientType
	Bcc  *RecipientType
}{
	GetEnumeration("From", "").ConvertToRecipientType(),
	GetEnumeration("To", "").ConvertToRecipientType(),
	GetEnumeration("Cc", "").ConvertToRecipientType(),
	GetEnumeration("Bcc", "").ConvertToRecipientType(),
}
