package domain

import (
	"context"
	"database/sql"

	uuid "github.com/satori/go.uuid"
)

type SmsTemplate struct {
	BaseEntitiy
	TemplateId uuid.UUID
	ProviderId uuid.UUID
	Content    string
	LanguageId uuid.UUID
}

func (SmsTemplate) TableName() string {
	return "sms_template"
}

type SmsTemplateRepository interface {
	Get(ctx context.Context) ([]*SmsTemplate, error)
	GetById(ctx context.Context, id uuid.UUID) (*SmsTemplate, error)
	GetByNotificationTypeId(ctx context.Context, notificationTypeId uuid.UUID) (*SmsTemplateQueryModel, error)
	Save(ctx context.Context, smsTemplate *SmsTemplate) error
	Delete(ctx context.Context, smsTemplate *SmsTemplate) error
}

type SmsTemplateQueryModel struct {
	ID                 uuid.UUID
	TemplateId         uuid.UUID
	TemplateName       sql.NullString
	NotificationTypeId uuid.UUID
	NotificationType   sql.NullString
	ProviderId         uuid.UUID
	ProviderName       sql.NullString
	LanguageId         uuid.UUID
	LanguageCode       sql.NullString
	Content            sql.NullString
}
