package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type NotificationType struct {
	BaseEntitiy
	Name        string
	Description string
	CategoryId  uuid.UUID
}

func (NotificationType) TableName() string {
	return "notification_type"
}

type NotificationTypeRepository interface {
	Get(ctx context.Context, name string) ([]*NotificationType, error)
	GetById(ctx context.Context, id uuid.UUID) (*NotificationType, error)
	Save(ctx context.Context, notificationType *NotificationType) error
	Delete(ctx context.Context, notificationType *NotificationType) error
}
