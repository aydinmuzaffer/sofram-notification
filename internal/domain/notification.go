package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type Notification struct {
	BaseEntitiy
	NotificationTypeId uuid.UUID
	Name               string
	Subject            string
	LanguageId         uuid.UUID
	ProviderId         uuid.UUID
	ChanelId           uuid.UUID
	BucketKey          string
}

func (Notification) TableName() string {
	return "notification"
}

type NotificationRepository interface {
	Save(ctx context.Context, notification *Notification) error
}
