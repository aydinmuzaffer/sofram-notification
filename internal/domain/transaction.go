package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type Transaction struct {
	BaseEntitiy
	NotificationId     uuid.UUID
	ProviderReturnCode string
	ProviderMessage    string
	ProviderMessageId  string
}

func (Transaction) TableName() string {
	return "transaction"
}

type TransactionRepository interface {
	Save(ctx context.Context, transaction *Transaction) error
}
