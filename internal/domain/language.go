package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type Language struct {
	BaseEntitiy
	Name string
	Code string
}

func (Language) TableName() string {
	return "language"
}

type LanguageRepository interface {
	Get(ctx context.Context, code string) ([]*Language, error)
	GetByCode(ctx context.Context, code string) (*Language, error)
	GetById(ctx context.Context, id uuid.UUID) (*Language, error)
	Save(ctx context.Context, language *Language) error
	Delete(ctx context.Context, language *Language) error
}
