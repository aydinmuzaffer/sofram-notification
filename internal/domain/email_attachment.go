package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type EmailAttachment struct {
	BaseEntitiy
	NotificationId uuid.UUID
	Name           string
	BucketKey      string
}

func (EmailAttachment) TableName() string {
	return "email_attachment"
}

type EmailAttachmentRepository interface {
	Save(ctx context.Context, attacment *EmailAttachment) error
}
