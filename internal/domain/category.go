package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type Category struct {
	BaseEntitiy
	Name        string
	Description string
}

func (Category) TableName() string {
	return "category"
}

type CategoryRepository interface {
	Get(ctx context.Context) ([]*Category, error)
	GetById(ctx context.Context, id uuid.UUID) (*Category, error)
	Save(ctx context.Context, categotry *Category) error
	Delete(ctx context.Context, categotry *Category) error
}
