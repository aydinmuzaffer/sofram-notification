package domain

import (
	"context"
)

type Chanel struct {
	Enumeration
}

func (Chanel) TableName() string {
	return "chanel"
}

type ChanelRepository interface {
	Get(ctx context.Context) ([]*Chanel, error)
}

func (p Enumeration) ConvertToChanel() *Chanel {
	return &Chanel{p}
}

var ChanelEnum = struct {
	Mail *Chanel
	Sms  *Chanel
}{
	GetEnumeration("Mail", "Mail chanel").ConvertToChanel(),
	GetEnumeration("Sms", "Sms chanel").ConvertToChanel(),
}
