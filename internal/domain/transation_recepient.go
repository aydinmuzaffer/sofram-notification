package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type TransactionRecipient struct {
	BaseEntitiy
	TransactionId   uuid.UUID
	Recipient       string
	RecipientTypeId uuid.UUID
}

func (TransactionRecipient) TableName() string {
	return "transaction_recipient"
}

type TransactionRecipientRepository interface {
	Save(ctx context.Context, transactionRecipient *TransactionRecipient) error
}
