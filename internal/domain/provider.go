package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type Provider struct {
	Enumeration
	ChanelId uuid.UUID
}

func (Provider) TableName() string {
	return "provider"
}

type ProviderRepository interface {
	Get(ctx context.Context) ([]*Provider, error)
}

func (p Enumeration) ConvertToProvider() *Provider {
	return &Provider{Enumeration: p}
}

var ProviderEnum = struct {
	AmazonSES *Provider
	Gmail     *Provider
	TwilloSMS *Provider
}{
	GetEnumeration("AmazonSES", "Mail with Amazon SES").ConvertToProvider(),
	GetEnumeration("Gmail", "Mail with Gmail").ConvertToProvider(),
	GetEnumeration("TwilloSMS", "Sms with Twillo").ConvertToProvider(),
}
