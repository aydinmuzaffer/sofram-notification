package domain

import (
	"context"
	"database/sql"

	uuid "github.com/satori/go.uuid"
)

type EmailTemplate struct {
	BaseEntitiy
	TemplateId    uuid.UUID
	EmailLayoutId uuid.UUID
	ProviderId    uuid.UUID
	BucketKey     string
	Location      string
	LanguageId    uuid.UUID
	Subject       string
}

func (EmailTemplate) TableName() string {
	return "email_template"
}

type EmailTemplateRepository interface {
	GetById(ctx context.Context, id uuid.UUID) (*EmailTemplate, error)
	GetByNotificationId(ctx context.Context, notificationTypeId uuid.UUID) (*EmailTemplateQueryModel, error)
	Save(ctx context.Context, emailTemplate *EmailTemplate) error
	Delete(ctx context.Context, emailTemplate *EmailTemplate) error
}

type EmailTemplateQueryModel struct {
	ID                     uuid.UUID
	TemplateId             uuid.UUID
	TemplateName           sql.NullString
	NotificationTypeId     uuid.UUID
	NotificationType       sql.NullString
	EmailLayoutId          uuid.UUID
	EmailLayoutDescription sql.NullString
	EmailLayoutLocation    sql.NullString
	ProviderId             uuid.UUID
	ProviderName           sql.NullString
	BucketKey              sql.NullString
	Location               sql.NullString
	LanguageId             uuid.UUID
	LanguageCode           sql.NullString
	Subject                sql.NullString
}
