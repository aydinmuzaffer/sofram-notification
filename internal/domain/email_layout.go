package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type EmailLayout struct {
	BaseEntitiy
	Name        string
	Description string
	BucketKey   string
	Location    string
}

func (EmailLayout) TableName() string {
	return "email_layout"
}

type EmailLayoutRepository interface {
	Get(ctx context.Context, name string) ([]*EmailLayout, error)
	GetById(ctx context.Context, id uuid.UUID) (*EmailLayout, error)
	Save(ctx context.Context, emailLayout *EmailLayout) error
	Delete(ctx context.Context, emailLayout *EmailLayout) error
}
