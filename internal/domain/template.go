package domain

import (
	"context"

	uuid "github.com/satori/go.uuid"
)

type Template struct {
	BaseEntitiy
	NotificationTypeId uuid.UUID
	Name               string
}

func (Template) TableName() string {
	return "template"
}

type TemplateRepository interface {
	Get(ctx context.Context, name string) ([]*Template, error)
	GetById(ctx context.Context, id uuid.UUID) (*TemplateQueryModel, error)
	GetByNotificationTypeId(ctx context.Context, notificationTypeId uuid.UUID) (*Template, error)
	Save(ctx context.Context, template *Template) error
	Delete(ctx context.Context, template *Template) error
}

type TemplateQueryModel struct {
	Id                 uuid.UUID                  `json:"id"`
	NotificationTypeId uuid.UUID                  `json:"notification_type_id"`
	NotificationType   string                     `json:"notification_type"`
	Name               string                     `json:"name"`
	Sms                []*SmsTemplateQueryModel   `json:"sms"`
	Email              []*EmailTemplateQueryModel `json:"email"`
}
