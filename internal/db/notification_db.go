package db

import (
	"fmt"

	_ "github.com/golang-migrate/migrate/v4/source/file"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	log "github.com/sirupsen/logrus"
)

type NotificationConnection struct {
	Db *gorm.DB
}

func NewNotificationConnection(host, port, user, dbName, password, sslMode string) (*NotificationConnection, error) {
	log.Info("Setting up new database connection")

	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		host,
		port,
		user,
		dbName,
		password,
		sslMode,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Info("connection refused for ", dsn)
		return &NotificationConnection{}, fmt.Errorf("could not connect to database: %w", err)
	}

	return &NotificationConnection{
		Db: db,
	}, nil
}
